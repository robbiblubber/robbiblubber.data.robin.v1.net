﻿using System;



namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This class represents a vehicle.</summary>
    [Entity("VEHICLES")][Super(SuperType.INTEGRATED, "Car", "Chaise", "Oxcart")]
    public abstract class Vehicle: Atom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Vehicle(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the time of the last change to the object.</summary>
        [ChangeTime(ColumnName = "JTIME")]
        protected DateTime TimeChanged { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the person ID.</summary>
        [Primary(Size = 24)]
        public string ID { get; protected set; }


        /// <summary>Gets or sets the vehicle weight.</summary>
        [Field]
        public int Weight { get; set; }
    }
}
