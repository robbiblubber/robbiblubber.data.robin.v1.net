﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Data.Robin.Base;
using Robbiblubber.Util.Collections;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This class provides a mutable object list.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class MutableObjectList<T>: ExtensibleObjectList<T>, IMutableObjectList<T>, IMutableList<T>, IExtensibleObjectList<T>, IExtensibleList<T>, IImmutableObjectList<T>, IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IEnumerable, __IObjectList
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="query">SQL query.</param>
        protected internal MutableObjectList(ISQLQuery query): base(query)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        protected internal MutableObjectList(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="list"></param>
        /// <param name="target"></param>
        protected internal MutableObjectList(object list, object target)
        {
            _List = list;
            _Target = target;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMutableList<T>                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        public virtual new T this[int i] 
        { 
            get { return base[i]; }
            set 
            {
                _Load();
                _Virgin = false;
                _Items[i] = value; 
            }
        }


        /// <summary>Clears the list.</summary>
        public virtual void Clear()
        {
            _Load();
            _Virgin = false;
            _Items.Clear();
        }


        /// <summary>Removes an item.</summary>
        /// <param name="item">Item.</param>
        public virtual bool Remove(T item)
        {
            _Load();
            _Virgin = false;
            bool rval = _Items.Remove(item);

            if(_List != null) { ((__ListDescriptor) _List).Propagate(true, item, _Target); }
            return rval;
        }


        /// <summary>Removes the item with the given index.</summary>
        /// <param name="i">Index.</param>
        public virtual void RemoveAt(int i)
        {
            _Load();
            _Virgin = false;

            T item = _Items[i];
            _Items.RemoveAt(i);

            if(_List != null) { ((__ListDescriptor) _List).Propagate(true, item, _Target); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IObjectList                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object virgin flag.</summary>
        bool __IObjectList.Virgin
        {
            get { return _Virgin; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>Adds an object to the list.</summary>
        /// <param name="obj">Object.</param>
        void __IObjectList.Add(object obj)
        {
            if(_Items != null) Add((T) obj);
        }


        /// <summary>Removes an object from the list.</summary>
        /// <param name="obj">Object.</param>
        void __IObjectList.Remove(object obj)
        {
            if(_Items != null)
            {
                if(_Items.Contains((T) obj)) { _Items.Remove((T) obj); }
            }
        }


        /// <summary>Copies the list to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Start index.</param>
        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            _Items.CopyTo(array, arrayIndex);
        }
    }
}
