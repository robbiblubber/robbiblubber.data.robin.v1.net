﻿using System;

using Robbiblubber.Data.DDL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>Classes that provide database DDL for locking implement this interface.</summary>
    public interface ILockBuilder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the lock DDL.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="tableName">Table name.</param>
        /// <returns>DDL.</returns>
        DDLBuilder GetDDL(string schemaName = null, string tableName = null);
    }
}
