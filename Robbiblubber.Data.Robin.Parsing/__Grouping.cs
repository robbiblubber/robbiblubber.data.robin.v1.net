﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements a basic implementation of the IGrouping interface.</summary>
    /// <typeparam name="TKey">Key type.</typeparam>
    /// <typeparam name="TElement">Element type.</typeparam>
    internal class __Grouping<TKey, TElement>: IGrouping<TKey, TElement>, IEnumerable<TElement>, __IGrouping, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Elements.</summary>
        private List<TElement> _Elements = new List<TElement>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="key">Key.</param>
        public __Grouping(TKey key)
        {
            Key = key;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IGrouping<TKey, TElement>                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the grouping key.</summary>
        public TKey Key
        {
            get; private set;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<TElement>                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this grouping.</summary>
        /// <returns>Enumerator.</returns>
        public IEnumerator<TElement> GetEnumerator()
        {
            return _Elements.GetEnumerator();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets an enumerator for this grouping.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Elements.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IGrouping                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the element list.</summary>
        /// <returns>Element list.</returns>
        IList __IGrouping.GetList()
        {
            return _Elements;
        }


        /// <summary>Gets the grouping key.</summary>
        /// <returns>Key.</returns>
        object __IGrouping.GetKey()
        {
            return Key;
        }
    }
}
