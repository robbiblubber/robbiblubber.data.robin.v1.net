﻿using System;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This enumeration defines method types.</summary>
    public enum QueryType: int
    {
        /// <summary>Unknown query.</summary>
        UNKNOWN = -1,
        /// <summary>WHERE query.</summary>
        WHERE = 0,
        /// <summary>SELECT query.</summary>
        SELECT = 1,
        /// <summary>SELECTMANY query.</summary>
        SELECTMANY = 2,
        /// <summary>AGGREGATE query.</summary>
        AGGREGATE = 3,
        /// <summary>ALL query.</summary>
        ALL = 4,
        /// <summary>ANY query.</summary>
        ANY = 5,
        /// <summary>AVERAGE query.</summary>
        AVERAGE = 6,
        /// <summary>CAST query.</summary>
        CAST = 7,
        /// <summary>CONCAT query.</summary>
        CONCAT = 8,
        /// <summary>COUNT query.</summary>
        COUNT = 9,
        /// <summary>DEFAULTIFEMPTY query.</summary>
        DEFAULTIFEMPTY = 10,
        /// <summary>DISTINCT query.</summary>
        DISTINCT = 11,
        /// <summary>ELEMENTAT query.</summary>
        ELEMENTAT = 12,
        /// <summary>ELEMENTATORDEFAULT query.</summary>
        ELEMENTATORDEFAULT = 13,
        /// <summary>EXCEPT query.</summary>
        EXCEPT = 14,
        /// <summary>FIRST query.</summary>
        FIRST = 15,
        /// <summary>FIRSTORDEFAULT query.</summary>
        FIRSTORDEFAULT = 16,
        /// <summary>GROUPBY query.</summary>
        GROUPBY = 17,
        /// <summary>GROUPJOIN query.</summary>
        GROUPJOIN = 18,
        /// <summary>INTERSECT query.</summary>
        INTERSECT = 19,
        /// <summary>JOIN query.</summary>
        JOIN = 20,
        /// <summary>LAST query.</summary>
        LAST = 21,
        /// <summary>LASTORDEFAULT query.</summary>
        LASTORDEFAULT = 22,
        /// <summary>LONGCOUNT query.</summary>
        LONGCOUNT = 23,
        /// <summary>MAX query.</summary>
        MAX = 24,
        /// <summary>MIN query.</summary>
        MIN = 25,
        /// <summary>OFTYPE query.</summary>
        OFTYPE = 26,
        /// <summary>ORDER BY query.</summary>
        ORDERBY = 27,
        /// <summary>ORDER BY DESCENDING query.</summary>
        ORDERBYDESCENDING = 28,
        /// <summary>REVERSE query.</summary>
        REVERSE = 29,
        /// <summary>SEQUENCEEQUAL query.</summary>
        SEQUENCEEQUAL = 30,
        /// <summary>SINGLE query.</summary>
        SINGLE = 31,
        /// <summary>SINGLEORDEFAULT query.</summary>
        SINGLEORDEFAULT = 32,
        /// <summary>SKIP query.</summary>
        SKIP = 33,
        /// <summary>SKIPWHILE query.</summary>
        SKIPWHILE = 34,
        /// <summary>SUM query.</summary>
        SUM = 35,
        /// <summary>TAKE query.</summary>
        TAKE = 36,
        /// <summary>TAKEWHILE query.</summary>
        TAKEWHILE = 37,
        /// <summary>THENBY query.</summary>
        THENBY = 38,
        /// <summary>THENBYDESCENDING query.</summary>
        THENBYDESCENDING = 39,
        /// <summary>UNION query.</summary>
        UNION = 40,
        /// <summary>UNION query.</summary>
        UNIONALL = 41,
        /// <summary>ZIP query.</summary>
        ZIP = 42
    }
}
