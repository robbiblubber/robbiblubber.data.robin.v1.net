﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements a typeless equality comparer wrapper for typed equality comparers.</summary>
    internal sealed class __EqualityComparer: IEqualityComparer<object>, IEqualityComparer
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Typed comparer.</summary>
        private object _Comparer;

        /// <summary>Equals method.</summary>
        private MethodInfo _Equals;

        /// <summary>GetHashCode method.</summary>
        private MethodInfo _GetHashCode;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="comparer">Typed comparer.</param>
        public __EqualityComparer(object comparer)
        {
            _Comparer = comparer;

            foreach(MethodInfo i in comparer.GetType().GetMethods())
            {
                if((i.Name == "Equals") && (i.GetParameters().Length == 2))
                {
                    _Equals = i;
                }
                else if((i.Name == "GetHashCode") && (i.GetParameters().Length == 1))
                {
                    _GetHashCode = i;
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEqualityComparer                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two instances.</summary>
        /// <param name="x">Instance.</param>
        /// <param name="y">Instance.</param>
        /// <returns>Returns TRUE if the instances are equal, otherwise returns FALSE.</returns>
        public new bool Equals(object x, object y)
        {
            return (bool) _Equals.Invoke(_Comparer, new object[] { x, y });
        }


        /// <summary>Returns a hash code for the object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Hash code.</returns>
        public int GetHashCode(object obj)
        {
            return (int) _GetHashCode.Invoke(_Comparer, new object[] { obj });
        }
    }
}
