﻿using System;
using System.Collections.Generic;
using System.Data;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>Cache query results implement this interface.</summary>
    public interface ICacheResult
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the cached object.</summary>
        object Object { get; }


        /// <summary>Gets if the cached object has been changed.</summary>
        bool HasChanges { get; }


        /// <summary>Gets if the cached object is current.</summary>
        bool IsCurrent { get; }


        /// <summary>Gets or sets the read time for the cached object.</summary>
        DateTime LastRead { get; }


        /// <summary>Gets a list of fields with changes.</summary>
        IEnumerable<string> ChangedFieldNames { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Updates the stored values for the cached object.</summary>
        void Update();


        /// <summary>Updates the stored values for the cached object.</summary>
        /// <param name="obj">Object instance.</param>
        void Update(object obj);


        /// <summary>Updates the stored values for the cached object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="re">Data.</param>
        /// <param name="force">Determines if refresh will ignore changes.</param>
        void Update(object obj, IDataReader re, bool force);


        /// <summary>Updates the stored values for the cached object.</summary>
        /// <param name="re">Data.</param>
        /// <param name="force">Determines if refresh will ignore changes.</param>
        void Update(IDataReader re, bool force);


        /// <summary>Removes the object from the cache.</summary>
        void Remove();


        /// <summary>Gets the old value of a field.</summary>
        /// <param name="field">Field.</param>
        /// <returns>Value.</returns>
        object GetOldValue(string field);
    }
}
