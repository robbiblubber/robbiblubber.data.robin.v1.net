﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements an internal grouping dictionary.</summary>
    internal sealed class __GroupingList: IEnumerable<__IGrouping>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        private List<__IGrouping> _Items = new List<__IGrouping>();

        /// <summary>Comparer.</summary>
        private IEqualityComparer<object> _Comparer;

        /// <summary>Element type.</summary>
        private Type _ElementType;


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="elementType">Element type.</param>
        /// <param name="comparer">Comparer.</param>
        public __GroupingList(Type elementType, IEqualityComparer<object> comparer)
        {
            _ElementType = elementType;
            _Comparer = comparer;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element.</summary>
        /// <param name="key">Key.</param>
        /// <returns></returns>
        public __IGrouping this[object key]
        {
            get
            {
                foreach(__IGrouping i in _Items)
                {
                    if(_Comparer.Equals(i.GetKey(), key)) return i;
                }

                __IGrouping rval = (__IGrouping) Activator.CreateInstance(typeof(__Grouping<,>).MakeGenericType(key.GetType(), _ElementType), key);
                _Items.Add(rval);

                return rval;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<__IGrouping>                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<__IGrouping> IEnumerable<__IGrouping>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }
    }
}
