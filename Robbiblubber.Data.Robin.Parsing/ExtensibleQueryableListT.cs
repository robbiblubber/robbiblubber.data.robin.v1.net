﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements an extensible list.</summary>
    /// <typeparam name="T"></typeparam>
    public class ExtensibleQueryableList<T>: ImmutableQueryableList<T>, IExtensibleList<T>, IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ExtensibleQueryableList(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="items">Items.</param>
        public ExtensibleQueryableList(IEnumerable items): base(items)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExtensibleList<T>                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        public virtual void Add(T item)
        {
            _Items.Add(item);
        }


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        public void AddRange(IEnumerable<T> items)
        {
            _Items.AddRange(items);
        }


        /// <summary>Inserts an item at a given position.</summary>
        /// <param name="i">Index.</param>
        /// <param name="item">Item.</param>
        public virtual void Insert(int i, T item)
        {
            _Items.Insert(i, item);
        }
    }
}
