﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This class provides a base implementation for framework objects.</summary>
    public class Atom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public Atom()
        {
            Manager.Initialize(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves this instance.</summary>
        public virtual void Save()
        {
            Manager.Save(this);
        }


        /// <summary>Deletes this instance.</summary>
        public virtual void Delete()
        {
            Manager.Delete(this);
        }


        /// <summary>Refreshes this instance.</summary>
        public virtual void Refresh()
        {
            Refresh(false);
        }


        /// <summary>Refreshes this instance.</summary>
        /// <param name="force">Determines if refresh will ignore changes.</param>
        public virtual void Refresh(bool force)
        {
            Manager.Refresh(force, this);
        }


        /// <summary>Locks this instance.</summary>
        public virtual void Lock()
        {
            Manager.Lock(this);
        }


        /// <summary>Unlocks this instance.</summary>
        public virtual void Unlock()
        {
            Manager.Release(this);
        }
    }
}
