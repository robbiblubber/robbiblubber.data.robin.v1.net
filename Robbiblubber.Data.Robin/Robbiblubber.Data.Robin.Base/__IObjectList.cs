﻿using System;
using System.Collections;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>Object lists implement this interface.</summary>
    internal interface __IObjectList: IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the object virgin flag.</summary>
        bool Virgin { get; }


        /// <summary>Gets the type of list elements.</summary>
        Type ElementType { get; }


        /// <summary>Gets the SQL query for this list.</summary>
        ISQLQuery Query { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an object to the list.</summary>
        /// <param name="obj">Object.</param>
        void Add(object obj);


        /// <summary>Removes an object from the list.</summary>
        /// <param name="obj">Object.</param>
        void Remove(object obj);
    }
}
