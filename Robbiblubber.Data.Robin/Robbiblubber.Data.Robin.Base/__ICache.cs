﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This interface extends ICache for framework cache implementations.</summary>
    internal interface __ICache: ICacheProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the cache dictionary.</summary>
        Dictionary<Type, Dictionary<object, __CacheElement>> CacheDictionary { get; }
    }
}
