﻿using System;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements a query provider for queryable lists.</summary>
    public class QueryProvider: IQueryProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent object.</summary>
        protected IEnumerable _Parent;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent object.</param>
        public QueryProvider(IEnumerable parent)
        {
            _Parent = parent;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IQueryProvider                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Constructs an IQueryable object that can evaluate the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>An IQueryable that can evaluate the query represented by the specified expression tree.</returns>
        public virtual IQueryable CreateQuery(Expression expression)
        {
            return (IQueryable) ExpressionParser.Handle(expression);
        }


        /// <summary>Constructs an IQueryable object that can evaluate the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>An IQueryable that can evaluate the query represented by the specified expression tree.</returns>
        public virtual IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return ((IQueryable<TElement>) CreateQuery(expression));
        }


        /// <summary>Executes the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>The value that results from executing the specified query.</returns>
        public virtual object Execute(Expression expression)
        {
            return ExpressionParser.Handle(expression);
        }


        /// <summary>Executes the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>The value that results from executing the specified query.</returns>
        public virtual TResult Execute<TResult>(Expression expression)
        {
            return (TResult) ExpressionParser.Handle(expression);
        }
    }
}
