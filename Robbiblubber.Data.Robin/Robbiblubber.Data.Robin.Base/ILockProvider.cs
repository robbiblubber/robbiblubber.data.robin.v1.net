﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>Lock providers implement this interface.</summary>
    public interface ILockProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a lock on an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the object has been successfully locked, otherwise returns FALSE.</returns>
        bool CreateLock(object obj);


        /// <summary>Creates a lock on an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the lock has has been successfully removed from the object, otherwise returns FALSE.</returns>
        bool ReleaseLock(object obj);


        /// <summary>Returns if the object is currently locked by this or another context.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="owned">Determines if the call returns TRUE for the calling context, otherwise returns FALSE.</param>
        /// <returns>Returns TRUE if the object is locked.</returns>
        bool IsLocked(object obj, bool owned = true);


        /// <summary>Returns if the object is currently locked by the calling context.</summary>
        /// <param name="obj">Object intance.</param>
        /// <returns>Returns TRUE if the object is locked by the calling context, otherwise returns FALSE.</returns>
        bool IsOwned(object obj);


        /// <summary>Returns if the object is not locked or locked by the calling context.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the object not locked or locked by the calling context, otherwise returns FALSE.</returns>
        bool CanLock(object obj);
    }
}
