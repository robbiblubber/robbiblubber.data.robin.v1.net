﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using Robbiblubber.Data.Robin.Parsing;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements a query provider for framework object lists.</summary>
    internal sealed class __QueryProvider: QueryProvider, IQueryProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent list.</param>
        public __QueryProvider(__IObjectList parent): base(parent)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a result list from a SQL query.</summary>
        /// <param name="query">SQL query.</param>
        /// <param name="t">Result type.</param>
        /// <returns>List instance.</returns>
        private IQueryable _CreateList(ISQLQuery query, Type t = null)
        {
            if(t == null) { t = ((__IObjectList) _Parent).ElementType; }
            t = typeof(ImmutableObjectList<>).MakeGenericType(t);

            foreach(ConstructorInfo i in t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if((i.GetParameters().Length > 0) && (i.GetParameters()[0].ParameterType == typeof(ISQLQuery)))
                {
                    return (IQueryable) i.Invoke(new object[] { query });
                }
            }
            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IQueryProvider                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Constructs an IQueryable object that can evaluate the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>An IQueryable that can evaluate the query represented by the specified expression tree.</returns>
        public override IQueryable CreateQuery(Expression expression)
        {
            if((((__IObjectList) _Parent).Query != null) && ((ExpressionParser.GetQueryType(expression) == QueryType.WHERE) || (ExpressionParser.GetQueryType(expression) == QueryType.ORDERBY)))
            {
                if(((MethodCallExpression) expression).Arguments.Count == 2)
                {
                    return _CreateList(Manager._EntityCache[_Parent.GetType().GetGenericArguments()[0]].ExpressionParser.Parse(((__IObjectList) _Parent).Query, expression));
                }
            }

            return (IQueryable) ExpressionParser.Handle(expression);
        }
    }
}
