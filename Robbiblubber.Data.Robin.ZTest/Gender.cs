﻿using System;



namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This enumeration defines genders.</summary>
    public enum Gender: int
    {
        /// <summary>Male gender.</summary>
        MALE = 0,
        /// <summary>Female gender.</summary>
        FEMALE = 1
    }
}
