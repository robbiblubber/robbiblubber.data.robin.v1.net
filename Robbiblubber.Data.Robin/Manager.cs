﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;

using Robbiblubber.Data.Robin.Base;
using Robbiblubber.Data.Robin.Timing;
using Robbiblubber.Data.Robin.Transactions;
using Robbiblubber.Util;
using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This class provides the Robin global framework manager.</summary>
    public static class Manager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static member                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Entity cache.</summary>
        internal static __EntityCache _EntityCache = new __EntityCache();

        /// <summary>Provider.</summary>
        internal static IProvider _Provider = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates the next primary key for a type.</summary>
        /// <param name="t">Type.</param>
        /// <returns>Next value.</returns>
        internal static object _CreateNextPrimary(Type t)
        {
            Type ft = _EntityCache[t].PrimaryKey.FieldType;
            
            if(ft == typeof(int))
            {
                Thread.Sleep(1);
                return int.Parse(DateTime.UtcNow.Month.ToString() +
                                 DateTime.UtcNow.Year.ToString().Substring(3) +
                                (DateTime.UtcNow.Day + ((int) DateTime.UtcNow.Second)).ToString() +
                               ((DateTime.UtcNow.Hour * 2) + ((int) (DateTime.UtcNow.Second / 9))).ToString() +
                                (DateTime.UtcNow.Minute + ((int) (DateTime.UtcNow.Millisecond / 65)).ToString()));
            }
            else if(ft == typeof(long))
            {
                Thread.Sleep(1);
                Random r = new Random();
                
                return long.Parse(DateTime.UtcNow.Month.ToString() +
                                  DateTime.UtcNow.Hour.ToString().PadLeft(2, '0') +
                                  DateTime.UtcNow.Year.ToString().Substring(3) +
                                  r.Next(100, 999).ToString() +
                                  DateTime.UtcNow.Day.ToString().PadLeft(2, '0') +
                                  DateTime.UtcNow.Minute.ToString().PadLeft(2, '0') +
                                  DateTime.UtcNow.Day.ToString().PadLeft(2, '0') +
                                  DateTime.UtcNow.Second.ToString().PadLeft(2, '0') +
                                  DateTime.UtcNow.Millisecond.ToString().PadLeft(3, '0'));
            }
            else { return StringOp.Unique(56); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the manager settings.</summary>
        public static ManagerSettings Settings
        {
            get;
        } = new ManagerSettings();


        /// <summary>Gets or sets the database provider.</summary>
        public static IProvider Provider
        {
            get { return _Provider; }
            set
            {
                _Provider = value;
                ServerTime.Instance = new ServerTime(value);
            }
        }


        /// <summary>Gets the builder that allows to create DDL and class sources.</summary>
        public static Builder Builder
        {
            get;
        } = new Builder();


        /// <summary>Gets or sets the object cache provider.</summary>
        public static ICacheProvider Cache
        {
            get; set;
        } = new SingleCache();
        

        /// <summary>Gets or sets the transaction provider.</summary>
        public static ITransactionProvider Transactions
        {
            get; set;
        }

        
        /// <summary>Gets or sets the lock provider.</summary>
        public static ILockProvider Locks
        {
            get; internal set;
        } = new DistributedLockProvider();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Registers a type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        public static void Register<T>()
        {
            _EntityCache.Register(typeof(T));
        }


        /// <summary>Registers all types for an assembly.</summary>
        /// <param name="t">Type.</param>
        public static void Register(Type t)
        {
            _EntityCache.Register(t);
        }


        /// <summary>Registers all types for an assembly.</summary>
        /// <param name="asm">Assembly.</param>
        public static void Register(Assembly asm)
        {
            _EntityCache.Register(asm);
        }


        /// <summary>Initializes a newly created object.</summary>
        /// <param name="obj">Object.</param>
        public static void Initialize(object obj)
        {
            _EntityCache[obj.GetType()].Initialize(obj, false);
        }


        /// <summary>Gets an object from the database.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="id">ID.</param>
        public static T Get<T>(object id)
        {
            return (T) _EntityCache[typeof(T)].Get(id);
        }


        /// <summary>Saves one or more objects to the database.</summary>
        /// <param name="obj">Objects.</param>
        public static void Save(params object[] obj)
        {
            foreach(object i in obj) { _EntityCache[i.GetType()].Save(i); }
        }


        /// <summary>Deletes one or more objects from the database.</summary>
        /// <param name="obj">Objects.</param>
        public static void Delete(params object[] obj)
        {
            foreach(object i in obj) { _EntityCache[i.GetType()].Delete(i); }
        }

        
        /// <summary>Refreshes one or more objects from the database.</summary>
        /// <param name="obj">Objects.</param>
        public static void Refresh(params object[] obj)
        {
            Refresh(false, obj);
        }


        /// <summary>Refreshes one or more objects from the database.</summary>
        /// <param name="force">Determines if a refresh is forced.</param>
        /// <param name="obj">Objects.</param>
        public static void Refresh(bool force, params object[] obj)
        {
            if(force)
            {
                foreach(object i in obj) { _EntityCache[i.GetType()].Refresh(i, force); }
            }
            else
            {
                List<Type> types = new List<Type>();
                foreach(object i in obj)
                {
                    if(!types.Contains(i.GetType())) { types.Add(i.GetType()); }
                }

                Refresh(types.ToArray());
            }
        }


        /// <summary>Refreshes all instances in the cache.</summary>
        public static void Refresh()
        {
            Refresh(Cache.Types);
        }


        /// <summary>Refreshes all cached instances of a type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        public static void Refresh<T>()
        {
            Refresh(typeof(T));
        }


        /// <summary>Refreshes all cached instances of a one or more types.</summary>
        /// <param name="types">Types.</param>
        public static void Refresh(params Type[] types)
        {
            foreach(Type t in types)
            {
                _EntityCache[t].Refresh();
            }
        }


        /// <summary>Locks one or more objects from the database.</summary>
        /// <param name="obj">Objects.</param>
        public static bool Lock(params object[] obj)
        {
            bool rval = true;
            foreach(object i in obj) { rval = (rval && Locks.CreateLock(i)); }

            if(!rval)
            {
                foreach(object i in obj) 
                {
                    if(Locks.IsOwned(i)) { Locks.ReleaseLock(i); }
                }
            }

            return rval;
        }


        /// <summary>Releases locks form one or more objects from the database.</summary>
        /// <param name="obj">Objects.</param>
        public static bool Release(params object[] obj)
        {
            bool rval = true;
            foreach(object i in obj) { rval = (rval && Locks.ReleaseLock(i)); }

            return rval;
        }


        /// <summary>Returns a list of all objects of a type from database.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <returns>Object list.</returns>
        public static IImmutableObjectList<T> Get<T>()
        {
            return new ImmutableObjectList<T>();
        }


        /// <summary>Returns a list of all objects of a type from database.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="expression">Expression.</param>
        /// <returns>Object list.</returns>
        public static IImmutableObjectList<T> Get<T>(Expression<Func<T, bool>> expression)
        {
            return new ImmutableObjectList<T>(expression);
        }
    }
}
