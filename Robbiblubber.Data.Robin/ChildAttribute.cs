﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a database-bound class as child of a super entity.</summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class ChildAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="key">Join key database column name.</param>
        public ChildAttribute(string key = null)
        {
            Key = key;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The join key database column name.</summary>
        public string Key = null;
    }
}
