﻿using System;
using System.Reflection;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class defines shared members for field and list descriptors.</summary>
    internal abstract class __FieldDescriptorBase
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Marshall method.</summary>
        protected MarshalField _Marshal;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <param name="member">Member.</param>
        protected __FieldDescriptorBase(__EntityDescriptor entity, MemberInfo member)
        {
            Entity = entity;
            Member = member;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the parent entity.</summary>
        public __EntityDescriptor Entity
        {
            get; private set;
        }


        /// <summary>Gets the member field.</summary>
        public MemberInfo Member
        {
            get; private set;
        }


        /// <summary>Gets if the member is a property.</summary>
        public bool IsProperty
        {
            get { return (Member is PropertyInfo); }
        }


        /// <summary>Gets the member field.</summary>
        public FieldInfo MemberField
        {
            get { return (FieldInfo) Member; }
        }


        /// <summary>Gets the member property.</summary>
        public PropertyInfo MemberProperty
        {
            get { return (PropertyInfo) Member; }
        }


        /// <summary>Gets the member type.</summary>
        public Type MemberType
        {
            get { return ((Member is PropertyInfo) ? ((PropertyInfo) Member).PropertyType : ((FieldInfo) Member).FieldType); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the field value for an instance.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="marshal">Determines if the result is marshalled.</param>
        /// <returns>Field value.</returns>
        public object GetValue(object obj, bool marshal)
        {
            if(obj == null) return null;
            if(marshal && (_Marshal != null)) { return _Marshal(MarshalDirection.OUT, GetValue(obj, false)); }
            
            return (IsProperty ? MemberProperty.GetValue(obj) : MemberField.GetValue(obj));
        }


        /// <summary>Sets the field value for an instance.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="value">Field value.</param>
        /// <param name="marshal">Determines if the result is marshalled.</param>
        public void SetValue(object obj, object value, bool marshal)
        {
            if(obj == null) return;
            if(marshal && (_Marshal != null)) { value = _Marshal(MarshalDirection.IN, value); }

            if(IsProperty)
            {
                MemberProperty.SetValue(obj, value);
            }
            else { MemberField.SetValue(obj, value); }
        }


        /// <summary>Marshals a value.</summary>
        /// <param name="direction">Direction.</param>
        /// <param name="value">Input value.</param>
        /// <returns>Return value.</returns>
        public object Marshal(MarshalDirection direction, object value)
        {
            if(_Marshal ==  null) { return value; }
            return _Marshal(direction, value);
        }
    }
}
