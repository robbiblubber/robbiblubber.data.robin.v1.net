﻿using System;



namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This class represents a tool.</summary>
    [Entity("TOOLS", "T")][Super(SuperType.VIRTUAL, "Hammer", "Saw")]
    public class Tool: Atom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Tool(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the time of the last change to the object.</summary>
        [ChangeTime(ColumnName = "JTIME")]
        protected DateTime TimeChanged { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the person ID.</summary>
        [Primary(Size = 24)]
        public string ID { get; protected set; }


        /// <summary>Gets or sets the tool weight.</summary>
        [Field]
        public int Weight { get; set; }
    }
}
