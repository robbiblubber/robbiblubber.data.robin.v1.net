﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This class represents a teacher.</summary>
    [Entity(TableName = "STUDENTS")][Child(Key = "KPERSON")]
    public class Student: Person, IComparable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Student(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the student grade.</summary>
        [Field]
        public int Grade { get; set; }


        /// <summary>Gets or sets the student class.</summary>
        [Foreign(ColumnName = "KCLASS")]
        public Class Class { get; set; }


        /// <summary>Gets the student courses.</summary>
        [ListMToN(RelationTableName = "STUDENT_COURSES", NearForeignKey = "KSTUDENT", FarForeignKey = "KCOURSE")]
        public IMutableObjectList<Course> Courses { get; protected set; }
    }
}
