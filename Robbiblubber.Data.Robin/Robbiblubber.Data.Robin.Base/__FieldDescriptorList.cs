﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements a list of field descriptors.</summary>
    internal class __FieldDescriptorList: IEnumerable<__FieldDescriptor>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item list.</summary>
        private Dictionary<string, __FieldDescriptor> _Items = new Dictionary<string, __FieldDescriptor>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a field descriptor for a given name.</summary>
        /// <param name="name">Name.</param>
        /// <returns>Field descriptor.</returns>
        public __FieldDescriptor this[string name]
        {
            get 
            {
                if(!_Items.ContainsKey(name)) { return null; }

                return _Items[name]; 
            }
        }


        /// <summary>Returns an ordered list of items.</summary>
        public IEnumerable<__FieldDescriptor> OrderedItems
        {
            get { return _Items.Values.OrderBy(m => m.SortKey); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns the added item.</returns>
        public __FieldDescriptor Add(__FieldDescriptor item)
        {
            int n = -1;
            foreach(__FieldDescriptor i in _Items.Values)
            {
                if(i._N > n) { n = i._N; }
            }
            item._N = (n + 1);
            _Items.Add(item.Name, item);
                        
            return item;
        }


        /// <summary>Gets a field by its column name.</summary>
        /// <param name="columnName">Column name.</param>
        /// <returns>Field.</returns>
        public __FieldDescriptor ByColumnName(string columnName)
        {
            columnName = columnName.ToUpper();
            foreach(__FieldDescriptor i in _Items.Values)
            {
                if(i.ColumnName.ToUpper() == columnName) return i;
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<__FieldDescriptor>                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<__FieldDescriptor> IEnumerable<__FieldDescriptor>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
