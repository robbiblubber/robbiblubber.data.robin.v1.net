﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Robin
{
    /// <summary>Immutable object lists implement this interface.</summary>
    /// <typeparam name="T"></typeparam>
    public interface IImmutableObjectList<T>: IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the list.</summary>
        void Refresh();
    }
}
