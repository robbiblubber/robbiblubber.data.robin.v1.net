﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

using Robbiblubber.Util;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class provides list information for m:n relations.</summary>
    internal sealed class __MToNDescriptor: __ListDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Far-side list.</summary>
        private __MToNDescriptor _FarList = null;

        /// <summary>Far-side list initialization flag.</summary>
        private bool _FarListInit = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <param name="member">Member.</param>
        public __MToNDescriptor(__EntityDescriptor entity, MemberInfo member): base(entity, member)
        {
            ListMToNAttribute a = member.GetCustomAttribute<ListMToNAttribute>();
            TableName = a.RelationTableName;
            TableAlias = __AliasManager.GetTableAlias("X");
            NearColumnName = a.NearForeignKey;
            FarColumnName = a.FarForeignKey;
        }

        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the relation table name.</summary>
        public string TableName { get; private set; }


        /// <summary>Gets the table alias.</summary>
        public string TableAlias { get; private set; }


        /// <summary>Gets the column name that references the descriptor parent entity.</summary>
        public string NearColumnName { get; private set; }


        /// <summary>Gets the column name that references the descriptor target entity.</summary>
        public string FarColumnName { get; private set; }


        /// <summary>Gets the far-side list for this list.</summary>
        public __MToNDescriptor FarList 
        { 
            get
            {
                if(!_FarListInit)
                {
                    _FarListInit = true;

                    foreach(__ListDescriptor i in TargetEntity.Lists)
                    {
                        if((i is __MToNDescriptor) && (((__MToNDescriptor) i).TargetEntity == Entity) && (((__MToNDescriptor) i).TableName.ToUpper() == TableName.ToUpper()))
                        {
                            _FarList = (__MToNDescriptor) i;
                            break;
                        }
                    }
                }

                return _FarList;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __ListDescriptor                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQL query for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>SQL query.</returns>
        public override ISQLQuery GetQuery(object obj)
        {
            return TargetEntity.Query.Clone().Where("EXISTS (SELECT 1 FROM " + TableName + " " + Manager.Provider.Parser.AsOperator + " " + TableAlias +
                                                    " WHERE " + TableAlias + "." + NearColumnName + " = " + Manager.Provider.Parser.ToBindVariableName(Entity.PrimaryKey.Name) +
                                                    " AND " + TableAlias + "." + FarColumnName + " = " + TargetEntity.PrimaryKey.ColumnExpression + ")")
                               .AddParameter(Manager.Provider.Parser.ToBindVariableName(Entity.PrimaryKey.Name), Entity.PrimaryKey.GetValue(obj, true));
        }


        /// <summary>Saves the list.</summary>
        /// <param name="obj">Object.</param>
        public override void Save(object obj)
        {
            IEnumerable list = (IEnumerable) GetValue(obj, false);

            if(list is __IObjectList)
            {
                if(((__IObjectList) list).Virgin) return;
            }

            List<object> dbv = new List<object>();
            List<object> obv = new List<object>();
            foreach(object i in list) { obv.Add(TargetEntity.PrimaryKey.GetValue(i, true)); }

            IDbCommand cmd = Manager.Provider.CreateCommand("SELECT " + FarColumnName + " FROM " + TableName +
                                                            " WHERE " + NearColumnName + " = " + Manager.Provider.Parser.ToBindVariableName("id"));
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("id"), Entity.PrimaryKey.GetValue(obj, true));

            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                dbv.Add(TargetEntity.PrimaryKey.DataType.GetValue(re, FarColumnName));
            }
            Disposal.Dispose(re, cmd);


            foreach(object i in obv)
            {
                if(!dbv.Contains(i))
                {
                    cmd = Manager.Provider.CreateCommand("INSERT INTO " + TableName + " (" + NearColumnName + ", " + FarColumnName + ") VALUES (" +
                                                         Manager.Provider.Parser.ToBindVariableName("navl") + ", " + Manager.Provider.Parser.ToBindVariableName("favl") + ")");
                    cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("navl"), Entity.PrimaryKey.GetValue(obj, true));
                    cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("favl"), i);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }

            foreach(object i in dbv)
            {
                if(!obv.Contains(i))
                {
                    cmd = Manager.Provider.CreateCommand("DELETE FROM " + TableName + " WHERE " + NearColumnName + " = " + Manager.Provider.Parser.ToBindVariableName("navl") +
                                                         " AND " + FarColumnName + " = " + Manager.Provider.Parser.ToBindVariableName("favl"));
                    cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("navl"), Entity.PrimaryKey.GetValue(obj, true));
                    cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("favl"), i);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
        }


        /// <summary>Initializes the list for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="force">Determines if the object needs to be re-initialized.</param>
        /// <param name="recurse">Determines if the method is called recursively.</param>
        public override void Initialize(object obj, bool force, bool recurse = false)
        {
            IEnumerable val = ((IEnumerable) GetValue(obj, false));
            base.Initialize(obj, force);

            if(force && (!recurse) && (FarList != null) && (val != null))
            {
                foreach(object i in val) { FarList.Initialize(i, true, true); }
            }
        }


        /// <summary>Adds an item to the far end list of an m:n relation if necessary.</summary>
        /// <param name="remove">Determines if the item is added (FALSE) or removed (TRUE).</param>
        /// <param name="target">Target object.</param>
        /// <param name="obj">Object.</param>
        public override void Propagate(bool remove, object target, object obj)
        {
            if(FarList != null) 
            {
                IEnumerable list = (IEnumerable) FarList.GetValue(target, false);

                if(list is __IObjectList)
                {
                    if(remove)
                    {
                        ((__IObjectList) list).Remove(obj);
                    }
                    else { ((__IObjectList) list).Add(obj); }
                }
                else if(list is IList) 
                {
                    if(remove)
                    {
                        ((__IObjectList) list).Remove(obj);
                    }
                    else { ((IList) list).Add(obj); }
                }
            }
        }
    }
}
