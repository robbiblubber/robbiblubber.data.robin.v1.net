﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a class or member that is not database-bound.</summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class IgnoreAttribute: Attribute
    {}
}
