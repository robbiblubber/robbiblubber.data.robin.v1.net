﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements a framework element comparer.</summary>
    internal class __ElementComparer: IComparer<object>, IComparer
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Field descriptor.</summary>
        private __FieldDescriptor _Field;

        /// <summary>Descending flag.</summary>
        private bool _Desc;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="field">Field descriptor.</param>
        /// <param name="desc">Descending flag.</param>
        public __ElementComparer(__FieldDescriptor field, bool desc)
        {
            _Field = field;
            _Desc = desc;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparer<object>                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two instances.</summary>
        /// <param name="x">Object being compared.</param>
        /// <param name="y">Object comparing to.</param>
        /// <returns>Result.</returns>
        public int Compare(object x, object y)
        {
            if(x == null) { return ((y == null) ? 0 : 1); }

            try
            {
                return ((IComparable) _Field.GetValue(x, false)).CompareTo(_Field.GetValue(y, false));
            }
            catch(Exception)
            {
                if(x.GetType() == _Field.Entity.Type)
                {
                    return ((y.GetType() == _Field.Entity.Type) ? 0 : -1);
                }
                else
                {
                    return ((y.GetType() == _Field.Entity.Type) ? 1 : 0);
                }
            }
        }
    }
}
