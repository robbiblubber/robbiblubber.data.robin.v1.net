﻿using System;



namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This class represents a person.</summary>
    [Entity(TableName = "PERSONS")][Super("Student", "Teacher")]
    public abstract class Person: Atom, IComparable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public Person(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the time of the last change to the object.</summary>
        [ChangeTime(ColumnName = "JTIME")]
        protected DateTime TimeChanged { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the person ID.</summary>
        [Primary(Size = 24)]
        public string ID { get; protected set; }


        /// <summary>Gets or sets the person's name.</summary>
        [Field(Size = 64)]
        public string Name { get; set; }


        /// <summary>Gets or sets the person's first name.</summary>
        [Field(Size = 64)]
        public string FirstName { get; set; }


        /// <summary>Gets or sets the person's gender.</summary>
        [Field]
        public Gender Gender { get; set; }


        /// <summary>Gets or sets the person's birth date.</summary>
        [Field(ColumnName = "BDATE")]
        public DateTime BirthDate { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.</summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>A value that indicates the relative order of the objects being compared.</returns>
        int IComparable.CompareTo(object obj)
        {
            if(!(obj is Person)) return -1;

            return Name.CompareTo(((Person) obj).Name);
        }
    }
}
