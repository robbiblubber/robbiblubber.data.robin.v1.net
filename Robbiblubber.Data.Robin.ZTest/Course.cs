﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This class represents a course.</summary>
    [Entity(TableName = "COURSES")]
    public class Course: Atom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the time of the last change to the object.</summary>
        [ChangeTime(ColumnName = "JTIME")]
        protected DateTime TimeChanged { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the course ID.</summary>
        [Primary(Size = 24)]
        public string ID { get; protected set; }


        /// <summary>Gets or sets the course name.</summary>
        [Field]
        public string Name { get; set; }


        /// <summary>Gets or sets if the course is active.</summary>
        [Field(ColumnName = "HACTIVE", DataType = DataType.BOOLEAN_INTEGER)]
        public bool Active { get; set; }


        /// <summary>Gets the students in this course.</summary>
        [ListMToN(RelationTableName = "STUDENT_COURSES", NearForeignKey = "KCOURSE", FarForeignKey = "KSTUDENT")]
        public IMutableObjectList<Student> Students { get; protected set; }
    }
}
