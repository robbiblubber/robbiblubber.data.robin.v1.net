﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.Robin.Timing;
using Robbiblubber.Util;
using Robbiblubber.Data.DDL;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements a database-bound lock provider on process level.</summary>
    public class DistributedLockProvider: ILockProvider, ILockBuilder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Current lock list.</summary>
        protected List<object> _Locks = new List<object>();

        /// <summary>Context token.</summary>
        protected string _Token = StringOp.Random();

        /// <summary>Lock table name.</summary>
        protected string _TableName;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="tableName">Table name.</param>
        public DistributedLockProvider(string schemaName = null, string tableName = null): this(new TimeSpan(0, 20, 0), schemaName, tableName)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="timeout">Lock timeout.</param>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="tableName">Table name.</param>
        public DistributedLockProvider(TimeSpan timeout, string schemaName = null, string tableName = null)
        {
            if(tableName == null) { tableName = "LOCKS"; }
            if(schemaName != null) { tableName = (schemaName + "." + tableName); }

            _TableName = tableName;
            Timeout = timeout;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the lock timeout.</summary>
        public virtual TimeSpan Timeout
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Purges timed out locks from the database.</summary>
        public virtual void Purge()
        {
            IDbCommand cmd = Manager.Provider.CreateCommand("DELETE FROM " + _TableName + " WHERE JTIME < " + Manager.Provider.Parser.ToBindVariableName("tim"));
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("tim"), (DateTime.Now - Timeout).ToServerTime());
            cmd.ExecuteNonQuery();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a lock on an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the object has been successfully locked, otherwise returns FALSE.</returns>
        protected virtual bool _CreateLock(object obj)
        {
            __EntityDescriptor entity = Manager._EntityCache[obj.GetType()];
            IDbCommand cmd = Manager.Provider.CreateCommand("INSERT INTO " + _TableName + " (JTABLE, JOBJECT, JLOCK, JTIME) VALUES (" +
                                                            Manager.Provider.Parser.ToBindVariableName("tab") + ", " +
                                                            Manager.Provider.Parser.ToBindVariableName("obj") + ", " +
                                                            Manager.Provider.Parser.ToBindVariableName("lck") + ", " +
                                                            Manager.Provider.Parser.CurrentTimestampFunction + ")");
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("tab"), entity.TableName);
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("obj"), entity.PrimaryKey.GetValue(obj, true).ToString());
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("lck"), _Token);

            bool rval = true;
            try
            {
                cmd.ExecuteNonQuery();
                _Locks.Add(obj);
            }
            catch(Exception) { rval = false; }

            cmd.Dispose();

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILockProvider                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a lock on an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the object has been successfully locked, otherwise returns FALSE.</returns>
        public virtual bool CreateLock(object obj)
        {
            if(obj == null) { return true; }
            if(_Locks.Contains(obj)) { return true; }

            if(_CreateLock(obj)) { return true; }

            Purge();
            return _CreateLock(obj);
        }


        /// <summary>Creates a lock on an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the lock has has been successfully removed from the object, otherwise returns FALSE.</returns>
        public virtual bool ReleaseLock(object obj)
        {
            if(_Locks.Contains(obj))
            {
                _Locks.Remove(obj);
                
                __EntityDescriptor entity = Manager._EntityCache[obj.GetType()];

                IDbCommand cmd = Manager.Provider.CreateCommand("DELETE FROM " + _TableName + " WHERE JTABLE = " + Manager.Provider.Parser.ToBindVariableName("tab") +
                                                                " AND JOBJECT = " + Manager.Provider.Parser.ToBindVariableName("obj"));
                cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("tab"), entity.TableName);
                cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("obj"), entity.PrimaryKey.GetValue(obj, true).ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }

            return true;
        }


        /// <summary>Returns if the object is currently locked by this or another context.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="owned">Determines if the call returns TRUE for the calling context.</param>
        /// <returns>Returns TRUE if the object is locked, otherwise returns FALSE.</returns>
        public virtual bool IsLocked(object obj, bool owned = true)
        {
            if(obj == null) { return false; }

            bool rval = false;
            __EntityDescriptor entity = Manager._EntityCache[obj.GetType()];

            IDbCommand cmd = Manager.Provider.CreateCommand("SELECT JLOCK FROM " + _TableName + " WHERE JTABLE = " + Manager.Provider.Parser.ToBindVariableName("tab") +
                                                                " AND JOBJECT = " + Manager.Provider.Parser.ToBindVariableName("obj"));
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("tab"), entity.TableName);
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("obj"), entity.PrimaryKey.GetValue(obj, true).ToString());

            IDataReader re = cmd.ExecuteReader();
            if(re.Read())
            {
                if(owned)
                {
                    rval = true;
                }
                else { rval = (re.GetString(0) != _Token); }
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Returns if the object is currently locked by the calling context.</summary>
        /// <param name="obj">Object intance.</param>
        /// <returns>Returns TRUE if the object is locked by the calling context, otherwise returns FALSE.</returns>
        public virtual bool IsOwned(object obj)
        {
            if(obj == null) { return false; }

            bool rval = false;
            __EntityDescriptor entity = Manager._EntityCache[obj.GetType()];

            IDbCommand cmd = Manager.Provider.CreateCommand("SELECT JLOCK FROM " + _TableName + " WHERE JTABLE = " + Manager.Provider.Parser.ToBindVariableName("tab") +
                                                                " AND JOBJECT = " + Manager.Provider.Parser.ToBindVariableName("obj"));
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("tab"), entity.TableName);
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("obj"), entity.PrimaryKey.GetValue(obj, true).ToString());

            IDataReader re = cmd.ExecuteReader();
            if(re.Read())
            {
                rval = (re.GetString(0) == _Token);
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Returns if the object is not locked or locked by the calling context.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the object not locked or locked by the calling context, otherwise returns FALSE.</returns>
        public virtual bool CanLock(object obj)
        {
            if(obj == null) { return true; }

            bool rval = true;
            __EntityDescriptor entity = Manager._EntityCache[obj.GetType()];

            IDbCommand cmd = Manager.Provider.CreateCommand("SELECT JLOCK FROM " + _TableName + " WHERE JTABLE = " + Manager.Provider.Parser.ToBindVariableName("tab") +
                                                                " AND JOBJECT = " + Manager.Provider.Parser.ToBindVariableName("obj"));
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("tab"), entity.TableName);
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("obj"), entity.PrimaryKey.GetValue(obj, true).ToString());

            IDataReader re = cmd.ExecuteReader();
            if(re.Read())
            {
                rval = (re.GetString(0) != _Token);
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILockBuilder                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the lock DDL.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="tableName">Table name.</param>
        /// <returns>DDL.</returns>
        public virtual DDLBuilder GetDDL(string schemaName = null, string tableName = null)
        {
            if(tableName == null) { tableName = "LOCKS"; }
            if(schemaName != null) { tableName = (schemaName + "." + tableName); }

            DDLBuilder rval = new DDLBuilder(Manager.Provider.Parser);
            rval.AddTable(tableName).AddColumn("JTABLE", SQLDataType.String(64)).AddColumn("JOBJECT", SQLDataType.String(128)).AddColumn("JLOCK", SQLDataType.String(128)).AddColumn("JTIME", SQLDataType.TIMESTAMP);
            rval.AddIndex(tableName, "JTABLE", "JOBJECT");

            return rval;
        }
    }
}
