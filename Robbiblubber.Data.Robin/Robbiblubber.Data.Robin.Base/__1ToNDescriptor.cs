﻿using System;
using System.Collections;
using System.Data;
using System.Reflection;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class provides list information for 1:n relations.</summary>
    internal sealed class __1ToNDescriptor: __ListDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Target column name.</summary>
        private string _TargetColumnName = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <param name="member">Member.</param>
        public __1ToNDescriptor(__EntityDescriptor entity, MemberInfo member): base(entity, member)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the target column name.</summary>
        public string TargetColumnName
        {
            get
            {
                if(_TargetColumnName == null)
                {
                    List1ToNAttribute attr = Member.GetCustomAttribute<List1ToNAttribute>();

                    if((attr == null) || ((attr.ColumnName == null) && (attr.FieldName == null)))
                    {
                        foreach(__FieldDescriptor i in TargetEntity.Fields) 
                        { 
                            if(i.FieldType == Entity.Type) { _TargetColumnName = i.ColumnName; break; }
                        }
                    }
                    else if(attr.ColumnName != null) 
                    {
                        _TargetColumnName = attr.ColumnName;
                    }
                    else
                    {
                        _TargetColumnName = TargetEntity.Fields[attr.FieldName].ColumnName;
                    }
                }

                return _TargetColumnName;
            }
        }


        /// <summary>Gets the target column expression.</summary>
        public string TargetColumnExpression
        {
            get { return TargetEntity.TableAlias + "." + TargetColumnName; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __ListDescriptor                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQL query for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>SQL query.</returns>
        public override ISQLQuery GetQuery(object obj)
        {
            return TargetEntity.Query.Clone().And(TargetColumnExpression + " = " + Manager.Provider.Parser.ToBindVariableName("tar"))
                               .AddParameter(Manager.Provider.Parser.ToBindVariableName("tar"), Entity.PrimaryKey.GetValue(obj, true));
        }


        /// <summary>Saves the list.</summary>
        /// <param name="obj">Object.</param>
        public override void Save(object obj)
        {
            IEnumerable list = (IEnumerable) GetValue(obj, false);
            __FieldDescriptor field = TargetEntity.Fields.ByColumnName(TargetColumnName);

            if(list is __IObjectList)
            {
                if(((__IObjectList) list).Virgin) return;
            }

            if(field == null)
            {
                foreach(object i in list)
                {
                    IDbCommand cmd = Manager.Provider.CreateCommand("UPDATE " + TargetEntity.TableName + " SET " + TargetColumnName + " = " + Manager.Provider.Parser.ToBindVariableName("v") +
                                                                    " WHERE " + TargetEntity.PrimaryKey.ColumnName + " = " + Manager.Provider.Parser.ToBindVariableName("id"));
                    cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("v"), Entity.PrimaryKey.GetValue(obj, true));
                    cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName("id"), TargetEntity.PrimaryKey.GetValue(i, true));
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
            else
            {
                foreach(object i in list)
                {
                    field.SetValue(i, obj, false);
                    TargetEntity.Save(i);
                }
            }
        }
    }
}
