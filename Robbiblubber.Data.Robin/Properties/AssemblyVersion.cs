﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;



[assembly: AssemblyCompany("robbiblubber.org")]
[assembly: AssemblyProduct("Robin ORM Framework")]
[assembly: AssemblyCopyright("© 2022 robbiblubber.org")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("3.2.0")]
[assembly: AssemblyFileVersion("3.2.0")]
