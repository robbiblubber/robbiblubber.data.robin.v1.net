﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using Robbiblubber.Data.Robin.Base;
using Robbiblubber.Data.Robin.Parsing;
using Robbiblubber.Util.Collections;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This class provides an immutable object list.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class ImmutableObjectList<T>: IImmutableObjectList<T>, IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IEnumerable, __IObjectList
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sorting.</summary>
        private List<IComparer> _Sorting = new List<IComparer>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item list.</summary>
        protected List<T> _Items = null;
        
        /// <summary>SQL Query.</summary>
        protected ISQLQuery _Query = null;

        /// <summary>List.</summary>
        protected object _List = null;

        /// <summary>Target object.</summary>
        protected object _Target = null;

        /// <summary>Query provider.</summary>
        protected object _Provider = null;

        /// <summary>Expression.</summary>
        protected Expression _Expression = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="query">SQL query.</param>
        protected internal ImmutableObjectList(ISQLQuery query)
        {
            _Query = query;
            _SetSorting();
        }


        /// <summary>Creates a new instance of this class.</summary>
        protected internal ImmutableObjectList(): this(Manager._EntityCache[typeof(T)].Query)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="expression">Expression.</param>
        protected internal ImmutableObjectList(Expression<Func<T, bool>> expression): this(Manager._EntityCache[typeof(T)].ExpressionParser.Parse(expression, QueryType.WHERE))
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="elements">Elements.</param>
        protected internal ImmutableObjectList(IEnumerable elements)
        {
            _Items = new List<T>();

            foreach(object i in elements)
            {
                if(i is T) { _Items.Add((T) i); }
            }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="list"></param>
        /// <param name="target"></param>
        protected internal ImmutableObjectList(object list, object target)
        {
            _List = list;
            _Target = target;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads list data.</summary>
        protected virtual void _Load()
        {
            if(_Items == null) 
            {
                _Items = new List<T>();
                if(_List != null) { _Query = ((__ListDescriptor) _List).GetQuery(_Target); }

                Manager._EntityCache[typeof(T)].Populate(_Items, _Query); 
            }
        }


        /// <summary>Sets the sorting for a list.</summary>
        protected void _SetSorting()
        {
            IEnumerable<Tuple<ISQLColumn, bool>> orderFields = null;
            if(_Query == null) return;

            try
            {
                foreach(FieldInfo i in _Query.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if(i.Name == "_OrderBy") { orderFields = (IEnumerable<Tuple<ISQLColumn, bool>>) i.GetValue(_Query); break; }
                }
            }
            catch(Exception) {}

            if(orderFields == null) return;

            foreach(Tuple<ISQLColumn, bool> i in orderFields)
            {
                __FieldDescriptor field = Manager._EntityCache[typeof(T)].Fields.ByColumnName(i.Item1.ColumnName);
                if(field != null) { _Sorting.Add(new __ElementComparer(field, i.Item2)); }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableObjectList                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the list.</summary>
        public virtual void Refresh()
        {
            _Items = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        public virtual T this[int i] 
        { 
            get 
            {
                _Load();
                return _Items[i]; 
            }
        }


        /// <summary>Gets the number of items in this collection.</summary>
        public virtual int Count 
        { 
            get 
            {
                _Load();
                return _Items.Count; 
            }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public virtual bool Contains(T item)
        {
            _Load();
            return _Items.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        public virtual int IndexOf(T item)
        {
            _Load();
            return _Items.IndexOf(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IObjectList                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object virgin flag.</summary>
        bool __IObjectList.Virgin
        { 
            get { return true; }
        }


        /// <summary>Gets the type of list elements.</summary>
        Type __IObjectList.ElementType 
        { 
            get { return typeof(T); }
        }


        /// <summary>Gets the SQL query for this list.</summary>
        ISQLQuery __IObjectList.Query 
        { 
            get { return _Query; }
        }


        /// <summary>Adds an object to the list.</summary>
        /// <param name="obj">Object.</param>
        void __IObjectList.Add(object obj)
        {}


        /// <summary>Removes an object from the list.</summary>
        /// <param name="obj">Object.</param>
        void __IObjectList.Remove(object obj)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IQueryable                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the expression tree associated with this instance.</summary>
        Expression IQueryable.Expression
        {
            get
            {
                if(_Expression == null) { _Expression = Expression.Constant(this); }
                return _Expression;
            }
        }

        /// <summary>Gets the type of elements that is returned when the expression tree is executed.</summary>
        Type IQueryable.ElementType
        {
            get { return typeof(T); }
        }

        /// <summary>Gets the query provider that is associated with this data source.</summary>
        IQueryProvider IQueryable.Provider
        {
            get
            {
                if(_Provider == null) { _Provider = new __QueryProvider(this); }

                return (IQueryProvider) _Provider;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator,</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            _Load();
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator,</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            _Load();
            return _Items.GetEnumerator();
        }
    }
}
