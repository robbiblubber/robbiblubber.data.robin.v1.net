﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements a default object equality comparer.</summary>
    internal sealed class __DefaultObjectEqualityComparer: IEqualityComparer<object>, IEqualityComparer
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEqualityComparer                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two instances.</summary>
        /// <param name="x">Instance.</param>
        /// <param name="y">Instance.</param>
        /// <returns>Returns TRUE if the instances are equal, otherwise returns FALSE.</returns>
        public new bool Equals(object x, object y)
        {
            if(x == null) return (y == null);
            return x.Equals(y);
        }


        /// <summary>Returns a hash code for the object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Hash code.</returns>
        public int GetHashCode(object obj)
        {
            return obj.GetHashCode();
        }
    }
}
