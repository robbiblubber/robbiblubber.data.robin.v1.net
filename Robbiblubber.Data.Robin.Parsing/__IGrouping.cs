﻿using System;
using System.Collections;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>Internal grouping interface.</summary>
    public interface __IGrouping: IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the element list.</summary>
        /// <returns>Element list.</returns>
        IList GetList();


        /// <summary>Gets the grouping key.</summary>
        /// <returns>Key.</returns>
        object GetKey();
    }
}
