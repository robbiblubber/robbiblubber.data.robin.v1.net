﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

using Robbiblubber.Data.Robin.Parsing;
using Robbiblubber.Util;
using Robbiblubber.Data.SQL;
using Robbiblubber.Data.Providers;
using System.IO;
using System.Linq;

namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class provies entity information.</summary>
    internal sealed class __EntityDescriptor: IParsableEntity, ISQLTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Expression parser.</summary>
        private ExpressionParser _ExpressionParser = null;

        /// <summary>Child entities.</summary>
        private __EntityDescriptor[] _ChildEntities = null;

        /// <summary>Type key indicator column alias.</summary>
        private string _TypeIndicatorColumnAlias = null;

        /// <summary>Primary key generator.</summary>
        internal CreateNextPrimary _NextPrimaryGenerator = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="t">Type.</param>
        internal __EntityDescriptor(Type t)
        {
            Type = t;

            EntityAttribute a = t.GetCustomAttribute<EntityAttribute>();
            TableName = (((a == null) || (a.TableName == null)) ? t.Name : a.TableName);
            TableAlias = (((a == null) || (a.TableAlias == null)) ? __AliasManager.GetTableAlias() : a.TableAlias);
            if(Manager.Settings.DatabaseNamesUppercase) { TableName = TableName.ToUpper(); }
            WhereClause = a?.WhereClause;

            SuperAttribute s = t.GetCustomAttribute<SuperAttribute>(false);
            if(s != null) { SuperType = s.SuperType; }

            if(t.GetCustomAttribute<UnjoinedAttribute>() == null)
            {
                if((Super = Manager._EntityCache.Register(t.BaseType)) != null)
                {
                    ChildAttribute j = t.GetCustomAttribute<ChildAttribute>();
                    JoinKey = (((j == null) || (j.Key == null)) ? Super.PrimaryKey.ColumnName : j.Key);

                    PrimaryKey = Super.PrimaryKey;
                }
            }

            _DiscoverFields();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the base query for this entity.</summary>
        private ISQLQuery _BaseQuery
        {
            get
            {
                ISQLQuery rval;
                if(IsJoined)
                {
                    rval = Super._BaseQuery;
                    rval.Join(this, FullJoinKey + " = " + (Super.IsChild ? Super.FullJoinKey : Super.PrimaryKey.FullColumnName));
                }
                else if(IsIntegrated)
                {
                    rval = new SQLQuery(Manager.Provider.Parser);
                    rval.From(Super);
                }
                else
                {
                    rval = new SQLQuery(Manager.Provider.Parser);
                    rval.From(this);
                }

                foreach(__FieldDescriptor i in _AllFields) { if(!(IsJoined && i.Inherited)) { rval.Select(i); } }
                rval.Where(WhereClause);

                return rval;
            }
        }


        /// <summary>Gets all fields, including fields from integrated child entities.</summary>
        private __FieldDescriptorList _AllFields
        {
            get
            {
                if(IsSuper && (SuperType == SuperType.INTEGRATED))
                {
                    __FieldDescriptorList rval = new __FieldDescriptorList();

                    foreach(__EntityDescriptor i in ChildEntities)
                    {
                        i.TableAlias = TableAlias;
                        foreach(__FieldDescriptor j in i.Fields)
                        {
                            if(!j.Inherited) { rval.Add(j); }
                        }
                    }

                    return rval;
                }

                return Fields;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the next primary key.</summary>
        /// <returns>Next primary key.</returns>
        private object _NextPrimary()
        {
            if(_NextPrimaryGenerator == null) { _NextPrimaryGenerator = new CreateNextPrimary(Manager._CreateNextPrimary); }
            return _NextPrimaryGenerator(Type);
        }


        /// <summary>Finds fields and lists for this entity.</summary>
        private void _DiscoverFields()
        {
            foreach(PropertyInfo i in Type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                _DiscoverField(i, Manager.Settings.RegisterUnmarkedProperties);
            }

            foreach(PropertyInfo i in Type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                _DiscoverField(i);
            }

            foreach(FieldInfo i in Type.GetFields(BindingFlags.Public | BindingFlags.Instance))
            {
                _DiscoverField(i, Manager.Settings.RegisterUnmarkedFields);
            }

            foreach(FieldInfo i in Type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                _DiscoverField(i);
            }
        }


        /// <summary>Discovers a field or list.</summary>
        /// <param name="member"></param>
        /// <param name="unmarked"></param>
        private void _DiscoverField(MemberInfo member, bool unmarked = false)
        {
            if(member is PropertyInfo) 
            {
                if((((PropertyInfo) member).GetMethod == null) || (((PropertyInfo) member).SetMethod == null)) return;
                unmarked = (unmarked && ((PropertyInfo) member).GetMethod.IsPublic);
            }
            else 
            { 
                unmarked = (unmarked && ((FieldInfo) member).IsPublic);
            }
                        
            if(member.GetCustomAttribute<List1ToNAttribute>() != null)
            {
                Lists.Add(new __1ToNDescriptor(this, member));
            }            
            else if(member.GetCustomAttribute<ListMToNAttribute>() != null)
            {
                Lists.Add(new __MToNDescriptor(this, member));
            }
            else if((__FieldDescriptor._GetFieldAttribute(member) != null) || unmarked) 
            {
                Fields.Add(new __FieldDescriptor(this, member));
            }
        }


        /// <summary>Refreshes selected instances.</summary>
        /// <param name="query">Query.</param>
        /// <param name="force">Determines if refresh will ignore changes.</param>
        private void _Refresh(ISQLQuery query, bool force)
        {
            IDbCommand cmd = Manager.Provider.CreateCommand(query);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                object pk = PrimaryKey.DataType.GetValue(re, PrimaryKey.ColumnAlias);
                if(Manager.Cache.Contains(Type, pk))
                {
                    Manager.Cache[Type, pk].Update(re, force);
                }
            }
            Disposal.Dispose(re, cmd);
        }


        /// <summary>Loads child entities into the class cache.</summary>
        private void _LoadChildEntities()
        {
            SuperAttribute a = Type.GetCustomAttribute<SuperAttribute>(false);
            if((a == null) || a.ChildEntities == null) return;

            Module[] m = Type.Assembly.GetModules(false);
            foreach(string i in a.ChildEntities)
            {
                Type t = Type.GetType(i);

                if(t == null)
                {
                    foreach(Module j in m)
                    {
                        foreach(Type k in j.FindTypes(Module.FilterTypeName, i))  {  Manager.Register(k); }
                    }
                }
                else
                {
                    Manager.Register(t);
                }
            }
        }


        /// <summary>Finds a type indicator for this entity.</summary>
        /// <param name="siblings">Sibling entities.</param>
        private void _FindTypeIndicator(__EntityDescriptor[] siblings)
        {
            if(SuperType == SuperType.INTEGRATED) return;

            foreach(__FieldDescriptor i in Fields)
            {
                if(i.Inherited) continue;

                bool found = true;
                foreach(__EntityDescriptor j in siblings)
                {
                    if(j == this) continue;
                    if(j.Fields.ByColumnName(i.ColumnName) != null) { found = false; break; }
                }

                if(found) { _TypeIndicatorColumnAlias = i.ColumnAlias; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQL query for this entity.</summary>
        public ISQLQuery Query
        {
            get
            {
                ISQLQuery rval = _BaseQuery;

                if(IsSuper)
                {
                    if(SuperType == SuperType.MATERIAL)
                    { 
                        foreach(__EntityDescriptor i in ChildEntities)
                        {
                            rval.LeftOuterJoin(i, PrimaryKey.FullColumnName + " = " + i.FullJoinKey);
                            rval.Select(i.JoinKeyColumn);

                            foreach(__FieldDescriptor j in i.Fields)
                            {
                                if(!j.Inherited) { rval.Select(j); }
                            }
                        }
                    }
                    else if(SuperType == SuperType.VIRTUAL)
                    {
                        ISQLQuery subq = null;
                        Dictionary<string, __FieldDescriptor> cols = new Dictionary<string, __FieldDescriptor>();
                        foreach(__EntityDescriptor i in ChildEntities)
                        {
                            foreach(__FieldDescriptor j in i.Fields) { if(!cols.ContainsKey(j.ColumnAlias)) { cols.Add(j.ColumnAlias, j); } }
                        }

                        foreach(__EntityDescriptor i in ChildEntities)
                        {
                            ISQLQuery q = new SQLQuery(Manager.Provider.Parser).From(i);
                            foreach(__FieldDescriptor j in cols.Values)
                            {
                                __FieldDescriptor f = i.Fields[j.Name];
                                q.Select(f == null ? new SQLField(null, "NULL", j.ColumnAlias) : (ISQLColumn) f);
                            }

                            foreach(__EntityDescriptor j in ChildEntities)
                            {
                                q.Select(new SQLField(null, j == i ? "1" : "NULL", j.JoinKeyAlias));
                            }

                            if(subq == null)
                            {
                                subq = q;
                            }
                            else
                            {
                                subq.UnionAll(q);
                            }
                        }

                        rval = new SQLQuery(Manager.Provider.Parser).From(new SQLTable("(" + subq + ")", TableAlias));
                        
                        foreach(string i in cols.Keys)
                        {
                            rval.Select(new SQLField(TableAlias, i, i));
                        }
                        foreach(__EntityDescriptor i in ChildEntities)
                        {
                            rval.Select(new SQLField(TableAlias, i.JoinKeyAlias, i.JoinKeyAlias));
                        }
                    }
                }

                return rval;
            }
        }


        /// <summary>Gets the expression parser for this entity.</summary>
        public ExpressionParser ExpressionParser
        {
            get 
            {
                if(_ExpressionParser == null)
                {
                    _ExpressionParser = new ExpressionParser(this);
                }

                return _ExpressionParser;
            }
        }


        /// <summary>Gets the entity type.</summary>
        public Type Type
        {
            get; private set;
        }


        /// <summary>Gets the primary key field.</summary>
        public __FieldDescriptor PrimaryKey
        {
            get; internal set;
        }


        /// <summary>Gets the change time field.</summary>
        public __FieldDescriptor ChangeTime
        {
            get; internal set;
        }


        /// <summary>Super entity type.</summary>
        public SuperType SuperType
        {
            get; private set;
        } = SuperType.MATERIAL;


        /// <summary>Gets if the entity is a child entity.</summary>
        public bool IsChild
        {
            get { return Super != null; }
        }


        /// <summary>Gets if the entity is joined to a material super entity.</summary>
        public bool IsJoined
        {
            get { return ((Super != null) && (Super.SuperType == SuperType.MATERIAL)); }
        }


        /// <summary>Gets if the entity is a child entity of an integrated super entity.</summary>
        public bool IsIntegrated
        {
            get { return ((Super != null) && (Super.SuperType == SuperType.INTEGRATED)); }
        }


        /// <summary>Gets if the entity is a supertype for child entities.</summary>
        public bool IsSuper
        {
            get { return ChildEntities.Length > 0; }
        }


        /// <summary>Gets the child entities for this entity.</summary>
        public __EntityDescriptor[] ChildEntities
        {
            get
            {
                if(_ChildEntities == null)
                {
                    _LoadChildEntities();
                    List<__EntityDescriptor> l = new List<__EntityDescriptor>();
                    if((SuperType == SuperType.VIRTUAL) && (!Type.IsAbstract)) { l.Add(this); }
                    if(SuperType == SuperType.INTEGRATED) { l.Add(this); }

                    foreach(__EntityDescriptor i in Manager._EntityCache)
                    {
                        if(i.Super == this) { l.Add(i); }
                    }

                    _ChildEntities = l.ToArray();

                    if(SuperType == SuperType.INTEGRATED)
                    {
                        foreach(__EntityDescriptor i in _ChildEntities) { i._FindTypeIndicator(_ChildEntities); }
                    }
                }

                return _ChildEntities;
            }
        }


        /// <summary>Gets the default where clause for this entity.</summary>
        public string WhereClause
        {
            get; private set;
        }


        /// <summary>Gets the base entity for this entity.</summary>
        public __EntityDescriptor Super
        {
            get; private set;
        }


        /// <summary>Gets the join (foreign) key.</summary>
        public string JoinKey
        {
            get; private set;
        }


        /// <summary>Gets the join (foreign) key including table alias..</summary>
        public string FullJoinKey
        {
            get { return TableAlias + "." + JoinKey; }
        }


        /// <summary>Gets the join (foreign) key expression.</summary>
        public string JoinKeyExpression
        {
            get { return FullJoinKey + Manager.Provider.Parser.AsOperator + JoinKeyAlias; }
        }


        /// <summary>Gets the join key alias.</summary>
        public string JoinKeyAlias
        {
            get { return "XK" + TableAlias; }
        }


        /// <summary>Gets the type key indicator column alias.</summary>
        public string TypeIndicatorColumnAlias
        {
            get { return ((_TypeIndicatorColumnAlias == null) ? JoinKeyAlias : _TypeIndicatorColumnAlias); }
        }


        /// <summary>Gets the join key as a SQL column.</summary>
        public ISQLColumn JoinKeyColumn
        {
            get { return new SQLField(TableAlias, JoinKey, JoinKeyAlias); }
        }


        /// <summary>Gets a list of field names.</summary>
        public IEnumerable<string> FieldNames
        {
            get
            {
                List<string> rval = new List<string>();
                foreach(__FieldDescriptor i in Fields) { rval.Add(i.Name); }

                return rval;
            }
        }


        /// <summary>Gets the entity fields.</summary>
        public __FieldDescriptorList Fields
        {
            get;
        } = new __FieldDescriptorList();


        /// <summary>Gets the entity lists.</summary>
        public __ListDescriptorList Lists
        {
            get;
        } = new __ListDescriptorList();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads an object from database.</summary>
        /// <param name="pk">Primary key.</param>
        /// <returns>Object.</returns>
        public object Get(object pk)
        {
            ICacheResult c = Manager.Cache[Type, pk];
            if(c.IsCurrent) { return c.Object; }

            ISQLQuery q = Query;

            if(IsJoined)
            {
                q.Where(TableAlias + "." + JoinKey + " = " + Manager.Provider.Parser.ToBindVariableName(TableAlias + "_" + PrimaryKey.Name));
            }
            else
            {
                q.Where(PrimaryKey.ColumnExpression + " = " + Manager.Provider.Parser.ToBindVariableName(TableAlias + "_" + PrimaryKey.Name));
            }
            q.AddParameter(Manager.Provider.Parser.ToBindVariableName(TableAlias + "_" + PrimaryKey.Name), pk);

            IDbCommand cmd = Manager.Provider.CreateCommand();
            cmd.CommandText = q.Text;
            foreach(var i in q.Parameters) { cmd.AddParameter(i.Item1, i.Item2); }

            IDataReader re = cmd.ExecuteReader();
            if(re.Read()) { c.Update(re); }
            Disposal.Dispose(re, cmd);

            return c.Object;
        }


        /// <summary>Loads an object from database.</summary>
        /// <param name="query">SQL query.</param>
        /// <returns>Object.</returns>
        public object Get(ISQLQuery query)
        {
            ICacheResult c = null;
            IDbCommand cmd = Manager.Provider.CreateCommand();
            cmd.CommandText = query.Text;
            foreach(var i in query.Parameters) { cmd.AddParameter(i.Item1, i.Item2); }

            IDataReader re = cmd.ExecuteReader();

            if(re.Read())
            {
                c = Manager.Cache[Type, PrimaryKey.DataType.GetValue(re, PrimaryKey.TableAlias)];
                c.Update(re);
            }
            Disposal.Dispose(re, cmd);

            return c?.Object;
        }


        /// <summary>Initilaizes an instance.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="force">Determines if the object needs to be re-initialized.</param>
        public void Initialize(object obj, bool force)
        {
            foreach(__ListDescriptor i in Lists)
            {
                i.Initialize(obj, force);
            }
        }


        /// <summary>Saves an instance.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="updateCache">Determines if the cache will be updated after saving.</param>
        public void Save(object obj, bool updateCache = true)
        {
            if(IsJoined || IsIntegrated) { Super.Save(obj, false); }            // save object base
            if(IsIntegrated) return;

            List<Tuple<__FieldDescriptor, object>> rem = new List<Tuple<__FieldDescriptor, object>>();
            if(Manager.Cache.Contains(obj))
            {                                                                   // object already persistent
                if(Manager.Cache[obj].HasChanges)
                {                                                               // only save objects with changes
                    if(ChangeTime != null) { ChangeTime.SetValue(obj, DateTime.Now, false); }

                    IDbCommand cmd = Manager.Provider.CreateCommand();
                    string fields = "";

                    foreach(string i in Manager.Cache[obj].ChangedFieldNames)
                    {
                        __FieldDescriptor f = Fields[i];

                        if(f == null) continue;
                        if(IsJoined && f.Inherited) continue;

                        if(fields.Length > 0) { fields += ", "; }
                        fields += (f.ColumnName + " = ");

                        if(f.IsChangeTime)
                        {
                            fields += Manager.Provider.Parser.CurrentTimestampFunction;
                        }
                        else if(f.IsForeignKey)
                        {                                                       // changed foreign key
                            if(Manager.Cache[obj].GetOldValue(f.Name) != null)
                            {                                                   // remember old value
                                rem.Add(new Tuple<__FieldDescriptor, object>(f, Manager.Cache[obj].GetOldValue(f.Name)));
                            }

                            fields += Manager.Provider.Parser.ToBindVariableName(f.Name);
                            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName(f.Name), f.TargetEntity.PrimaryKey.GetValue(f.GetValue(obj, false), true));
                        }
                        else
                        {
                            fields += Manager.Provider.Parser.ToBindVariableName(f.Name);
                            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName(f.Name), f.GetValue(obj, true));
                        }
                    }

                    if(!string.IsNullOrWhiteSpace(fields))
                    {                                                           // write changes
                        cmd.CommandText = "UPDATE " + TableName + " SET " + fields + " WHERE " +
                                          (IsChild ? JoinKey : PrimaryKey.ColumnName) + " = " + Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name);
                        cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name), PrimaryKey.GetValue(obj, true));
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
            }
            else
            {                                                                   // save new object
                IDbCommand cmd = Manager.Provider.CreateCommand();
                string fields = "";
                string values = "";

                if(IsJoined)
                {
                    fields += JoinKey;
                    values += Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name);
                    cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name), PrimaryKey.GetValue(obj, true));
                }
                else if(ClassOp.IsDefault(PrimaryKey.GetValue(obj, false))) { PrimaryKey.SetValue(obj, _NextPrimary(), false); }

                foreach(__FieldDescriptor i in _AllFields)
                {
                    if(IsJoined && i.Inherited) continue;
                    if(i.IsAutoIncrement) continue;

                    if(fields.Length > 0) { fields += ", "; values += ", "; }
                    fields += i.ColumnName;

                    if((SuperType == SuperType.INTEGRATED) && (!i.FieldExists(obj)))
                    {
                        values += "NULL";
                    }
                    else if(i.IsChangeTime)
                    {
                        values += Manager.Provider.Parser.CurrentTimestampFunction;
                    }
                    else if(i.IsForeignKey)
                    {
                        values += Manager.Provider.Parser.ToBindVariableName(i.Name);
                        cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName(i.Name), i.TargetEntity.PrimaryKey.GetValue(i.GetValue(obj, false), true));
                    }
                    else
                    {
                        values += Manager.Provider.Parser.ToBindVariableName(i.Name);
                        cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName(i.Name), i.GetValue(obj, true));
                    }
                }

                cmd.CommandText = "INSERT INTO " + TableName + " (" + fields + ") VALUES (" + values + ")";
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    DumpParams(cmd);
                    throw ex;
                }
                cmd.Dispose();

                if(PrimaryKey.IsAutoIncrement)
                {
                    object id = Manager.Provider.LastAutoValue();

                    if(PrimaryKey.MemberType == typeof(int))          { id = Convert.ToInt32(id);   }
                    else if(PrimaryKey.MemberType == typeof(uint))    { id = Convert.ToUInt32(id);  }
                    else if(PrimaryKey.MemberType == typeof(short))   { id = Convert.ToInt16(id);   }
                    else if(PrimaryKey.MemberType == typeof(ushort))  { id = Convert.ToUInt16(id);  }
                    else if(PrimaryKey.MemberType == typeof(float))   { id = Convert.ToSingle(id);  }
                    else if(PrimaryKey.MemberType == typeof(double))  { id = Convert.ToDouble(id);  }
                    else if(PrimaryKey.MemberType == typeof(decimal)) { id = Convert.ToDecimal(id); }
                    else if(PrimaryKey.MemberType == typeof(string))  { id = Convert.ToSingle(id);  }

                    PrimaryKey.SetValue(obj, id, false);
                }
            }

            foreach(__FieldDescriptor i in Fields)
            {                                                                   // add object to foreign key lists
                if(i.IsForeignKey)
                {
                    if(i.GetValue(obj, false) == null) continue;

                    foreach(__ListDescriptor j in i.TargetEntity.Lists)
                    {
                        if((j is __1ToNDescriptor) && (((__1ToNDescriptor) j).TargetColumnName.ToUpper() == i.ColumnName.ToUpper()))
                        {
                            object tar = j.GetValue(i.GetValue(obj, false), false);

                            if(tar is __IObjectList)
                            {
                                ((__IObjectList) tar).Add(obj);
                            }
                            else if(tar is IList)
                            {
                                if(!((IList) tar).Contains(obj)) { ((IList) tar).Add(obj); }
                            }
                            break;
                        }
                    }
                }
            }

            foreach(Tuple<__FieldDescriptor, object> i in rem)
            {                                                                   // remove object from old foreign key lists
                foreach(__ListDescriptor j in i.Item1.TargetEntity.Lists)
                {
                    if((j is __1ToNDescriptor) && (((__1ToNDescriptor) j).TargetColumnName.ToUpper() == i.Item1.ColumnName.ToUpper()))
                    {
                        object tar = j.GetValue(i.Item2, false);

                        if(tar is __IObjectList)
                        {
                            ((__IObjectList) tar).Remove(obj);
                        }
                        else if(tar is IList)
                        {
                            if(((IList) tar).Contains(obj)) { ((IList) tar).Remove(obj); }
                        }
                        break;
                    }
                }
            }

            if(updateCache) 
            {                                                                   // write lists and update cache
                foreach(__ListDescriptor i in Lists) { i.Save(obj); }
                Manager.Cache[obj].Update(); 
            }
        }


        
        /// <summary>Deletes an instance.</summary>
        /// <param name="obj">Object instance.</param>
        public void Delete(object obj)
        {
            IDbCommand cmd = Manager.Provider.CreateCommand("DELETE FROM " + TableName + " WHERE " + PrimaryKey.ColumnName + " = " + Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name));
            cmd.AddParameter(Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name), PrimaryKey.GetValue(obj, true));
            cmd.ExecuteNonQuery();
            cmd.Dispose();

            if(IsChild) { Super.Delete(obj); }

            Manager.Cache[obj].Remove();
        }


        /// <summary>Refreshes all cached instances of this entity.</summary>
        public void Refresh()
        {
            ISQLQuery q = Query.Clone();
            if(ChangeTime != null)
            {
                q.Where(ChangeTime.ColumnExpression + " > " + Manager.Provider.Parser.ToBindVariableName(ChangeTime.ColumnAlias));
                q.AddParameter(Manager.Provider.Parser.ToBindVariableName(ChangeTime.ColumnAlias), Manager.Cache.GetReadTime(Type));
            }

            _Refresh(q, false);
        }


        /// <summary>Refreshes an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="force">Determines if refresh will ignore changes.</param>
        public void Refresh(object obj, bool force)
        {
            _Refresh(Query.Clone().Where(PrimaryKey.ColumnExpression + " = " + Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name)).AddParameter(Manager.Provider.Parser.ToBindVariableName(PrimaryKey.Name), PrimaryKey.GetValue(obj, true)), force);
        }


        /// <summary>Gets an instance from a database reader.</summary>
        /// <param name="re">Database reader.</param>
        /// <returns>Object instance.</returns>
        public object GetInstance(IDataReader re)
        {
            Type actualType = Type;

            if(IsSuper)
            {
                foreach(__EntityDescriptor i in ChildEntities)
                {
                    int ordinal = re.GetOrdinal(i.TypeIndicatorColumnAlias);
                    if((ordinal >= 0) && (!re.IsDBNull(ordinal))) { actualType = i.Type; break; }
                }
            }

            ICacheResult rval = Manager.Cache[actualType, PrimaryKey.DataType.GetValue(re, PrimaryKey.ColumnAlias)];
            rval.Update(re);

            return rval.Object;
        }



        /// <summary>Populates a list from a database query.</summary>
        /// <param name="list">List.</param>
        /// <param name="query">SQL query.</param>
        public void Populate(IList list, ISQLQuery query)
        {
            IDbCommand cmd = Manager.Provider.CreateCommand(query.Text);            
            foreach(Tuple<string, object> i in query.Parameters) 
            { 
                if((i.Item2 != null) && (cmd.GetParameterType(i.Item2) == DbType.Object))
                {
                    cmd.AddParameter(i.Item1, Manager._EntityCache[i.Item2.GetType()].PrimaryKey.GetValue(i.Item2, true));
                }
                else
                {
                    cmd.AddParameter(i.Item1, i.Item2);
                }
            }

            IDataReader re = cmd.ExecuteReader();
            
            while(re.Read())
            {
                list.Add(GetInstance(re));
            }
            Disposal.Dispose(re, cmd);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISQLTable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the entity table name.</summary>
        public string TableName
        {
            get; private set;
        }


        /// <summary>Gets the table alias.</summary>
        public string TableAlias
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IParsableEntity                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database provider for this entity.</summary>
        IProvider IParsableEntity.Provider 
        { 
            get { return Manager.Provider; }
        }


        /// <summary>Creates a query for this instance.</summary>
        /// <returns>SQL query.</returns>
        ISQLQuery IParsableEntity.CreateQuery()
        {
            return Query.Clone();
        }


        /// <summary>Gets a field by its name.</summary>
        /// <param name="name">Field name.</param>
        /// <returns></returns>
        IParsableField IParsableEntity.GetField(string name)
        {
            return Fields[name];
        }





        /* DEBUG */
        public static void DumpParams(IDbCommand cmd)
        {
            string dump = "";
            string m = cmd.CommandText;
            string n = cmd.CommandText;

            List<IDataParameter> v = new List<IDataParameter>();
            
            foreach(IDataParameter i in cmd.Parameters)
            {
                dump += i.ParameterName.PadRight(18);
                dump += i.Value.GetType().Name.PadRight(12);
                dump += i.Value.ToString();
                dump += "\n";

                v.Add(i);
            }

            foreach(IDataParameter i in v.OrderByDescending(y => y.ParameterName.Length))
            {
                n = n.Replace(i.ParameterName, "?");
                if (i.Value is string)
                {
                    m = m.Replace(i.ParameterName, "'" + i.Value + "'");
                }
                else if (i.Value is DBNull)
                {
                    m = m.Replace(i.ParameterName, "NULL");
                }
                else
                {
                    m = m.Replace(i.ParameterName, i.Value.ToString());
                }
            }

            dump +=  "\n\n" + cmd.CommandText + "\n" + m + "\n" + n;

            File.WriteAllText(@"C:\home\dump.txt", dump);
        }
    }
}
