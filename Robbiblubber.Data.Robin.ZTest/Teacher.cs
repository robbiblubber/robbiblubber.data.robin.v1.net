﻿using System;



namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This class represents a teacher.</summary>
    [Entity(TableName = "TEACHERS")][Child(Key = "KPERSON")]
    public class Teacher: Person, IComparable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Teacher(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the hire date for this teacher.</summary>
        [Field(ColumnName = "HDATE")]
        public DateTime HireDate { get; set; }


        /// <summary>Gets or sets the teacher's salary.</summary>
        [Field]
        public int Salary { get; set; }


        [List1ToN]
        public IExtensibleObjectList<Class> Classes { get; private set; }
    }
}
