﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using Robbiblubber.Data.DDL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements the DDL builder.</summary>
    public sealed class Builder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        internal Builder()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends DDL for an entity to a string.</summary>
        /// <param name="entity">Entity.</param>
        /// <param name="s">String builder.</param>
        /// <param name="schemaName">Schema name.</param>
        private void _GetDDL(__EntityDescriptor entity, StringBuilder s, string schemaName)
        {
            if(entity == null) return;

            if(string.IsNullOrWhiteSpace(schemaName))
            {
                schemaName = "";
            }
            else
            {
                schemaName = schemaName.Trim();
                if(!schemaName.EndsWith(".")) { schemaName += "."; }
            }

            DDLTable ddl = DDLBuilder.CreateTable(Manager.Provider.Parser, schemaName + entity.TableName);
            if(entity.IsChild)
            {
                ddl.AddColumn(entity.JoinKey, entity.Super.PrimaryKey.DataType, entity.Super.PrimaryKey.IsNotNull);
                ddl.AddPrimaryKey(entity.JoinKey);
                ddl.AddForeignKey(entity.JoinKey, entity.Super.TableName, entity.Super.PrimaryKey.ColumnName, true);
            }

            foreach(__FieldDescriptor i in entity.Fields.OrderedItems)
            {
                if(i.Inherited) continue;

                ddl.AddColumn(i.ColumnName, i.DataType, i.IsNotNull);

                if(i.IsPrimaryKey)
                {
                    ddl.AddPrimaryKey(i.ColumnName);
                }
                if(i.IsForeignKey)
                {
                    ddl.AddForeignKey(i.ColumnName, i.TargetEntity.TableName, i.TargetEntity.PrimaryKey.ColumnName);
                }
                if(i.IsIndexed)
                {
                    ddl.AddIndex(i.ColumnName);
                }
            }

            foreach(string i in ddl.Text)
            {
                s.AppendLine(i + ";");
            }
            s.AppendLine();
        }


        /// <summary>Appends DDL for m:n tables to a string.</summary>
        /// <param name="types">Types.</param>
        /// <param name="s">String builder.</param>
        /// <param name="schemaName">Schema name.</param>
        private void _GetMToNDDL(Type[] types, StringBuilder s, string schemaName)
        {
            List<string> xtab = new List<string>();

            foreach(Type i in types)
            {
                foreach(__ListDescriptor j in Manager._EntityCache[i].Lists)
                {
                    if(j is __MToNDescriptor)
                    {
                        if(!xtab.Contains(((__MToNDescriptor) j).TableName))
                        {
                            xtab.Add(((__MToNDescriptor) j).TableName);

                            DDLTable ddl = DDLBuilder.CreateTable(Manager.Provider.Parser, ((__MToNDescriptor) j).TableName);

                            ddl.AddColumn(((__MToNDescriptor) j).NearColumnName, j.Entity.PrimaryKey.DataType, true);
                            ddl.AddColumn(((__MToNDescriptor) j).FarColumnName, j.TargetEntity.PrimaryKey.DataType, true);

                            ddl.AddPrimaryKey(((__MToNDescriptor) j).NearColumnName, ((__MToNDescriptor) j).FarColumnName);
                            ddl.AddForeignKey(((__MToNDescriptor) j).NearColumnName, j.Entity.TableName, j.Entity.PrimaryKey.ColumnName, true);
                            ddl.AddForeignKey(((__MToNDescriptor) j).FarColumnName, j.TargetEntity.TableName, j.TargetEntity.PrimaryKey.ColumnName, true);
                            foreach(string k in ddl.Text)
                            {
                                s.AppendLine(k + ";");
                            }
                            s.AppendLine();
                        }
                    }
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the DDL for a type.</summary>
        /// <param name="schemaName">Schema name.</param>
        public string GetDDL<T>(string schemaName)
        {
            return GetDDL(schemaName, typeof(T));
        }


        /// <summary>Gets the DDL for a number of types.</summary>
        /// <param name="types">Types.</param>
        public string GetDDL(params Type[] types)
        {
            return GetDDL(null, types);
        }


        /// <summary>Gets the DDL for a number of types.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="types">Types.</param>
        public string GetDDL(string schemaName, params Type[] types)
        {
            StringBuilder rval = new StringBuilder();

            foreach(Type i in types)
            {
                _GetDDL(Manager._EntityCache[i], rval, schemaName);
                rval.AppendLine();
            }

            _GetMToNDDL(types, rval, schemaName);

            if(Manager.Locks is ILockBuilder)
            {
                rval.AppendLine(((ILockBuilder) Manager.Locks).GetDDL(schemaName).ToString());
                rval.AppendLine();
            }

            return rval.ToString();
        }


        /// <summary>Gets the DDL for all types in an assembly.</summary>
        /// <param name="schemaName">Schema name.</param>
        /// <param name="asm">Assembly.</param>
        public string GetDDL(Assembly asm, string schemaName = null)
        {
            return GetDDL(schemaName, asm.GetTypes());
        }


        /// <summary>Gets the DDL for all registered types.</summary>
        /// <param name="schemaName">Schema name.</param>
        public string GetDDL(string schemaName = null)
        {
            List<Type> t = new List<Type>();

            foreach(__EntityDescriptor i in Manager._EntityCache)
            {
                t.Add(i.Type);
            }

            return GetDDL(schemaName, t.ToArray());
        }
    }
}
