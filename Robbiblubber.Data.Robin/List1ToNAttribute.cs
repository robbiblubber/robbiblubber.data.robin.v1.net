﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a list as part of a database-bound 1:n relation.</summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class List1ToNAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>The target database column name.</summary>
        public string ColumnName = null;
        
        /// <summary>The target field name.</summary>
        public string FieldName = null;
    }
}
