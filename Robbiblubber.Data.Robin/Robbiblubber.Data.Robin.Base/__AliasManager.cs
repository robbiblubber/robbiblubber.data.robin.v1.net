﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class generates unique table aliases.</summary>
    internal static class __AliasManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List of used aliases.</summary>
        private static List<string> _UsedAliases = new List<string>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an alias for a data table.</summary>
        /// <returns>Alias name.</returns>
        public static string GetTableAlias(string prefix = "T")
        {
            int i = 0;
            while(true)
            {
                string rval = (prefix + (++i));
                if(!_UsedAliases.Contains(rval)) 
                {
                    _UsedAliases.Add(rval);
                    return rval; 
                }
            }
        }
    }
}
