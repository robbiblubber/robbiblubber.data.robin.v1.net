﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>Cache providers implement this interface.</summary>
    public interface ICacheProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the cache consistency options.</summary>
        CacheConsistency Consitency { get; set; }


        /// <summary>Gets an array of types currently cached.</summary>
        Type[] Types { get; }


        /// <summary>Gets or sets the cache time-to-live.</summary>
        TimeSpan TTL { get; set; }


        /// <summary>Returns a cache result for a given object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Cache result.</returns>
        ICacheResult this[object obj] { get; }


        /// <summary>Returns a cache result for an object of a given type with the given primary key.</summary>
        /// <param name="t">Type.</param>
        /// <param name="pk">Primary key.</param>
        /// <returns>Cache result.</returns>
        ICacheResult this[Type t, object pk] { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if the cache contains a given object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the cache contains the object, otherwise returns FALSE.</returns>
        bool Contains(object obj);


        /// <summary>Returns if the cache contains an object of a given type with the given primary key.</summary>
        /// <param name="t">Type.</param>
        /// <param name="pk">Primary key.</param>
        /// <returns>Returns TRUE if the cache contains a matching object, otherwise returns FALSE.</returns>
        bool Contains(Type t, object pk);


        /// <summary>Returns the last read time of the cache elements of a type.</summary>
        /// <param name="t">Type.</param>
        /// <returns>Last read time.</returns>
        DateTime GetReadTime(Type t);
    }
}
