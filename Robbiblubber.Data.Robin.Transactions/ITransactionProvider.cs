﻿using System;
using System.Data;

using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.Robin.Transactions
{
    /// <summary>Transaction providers implement this interface.</summary>
    public interface ITransactionProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the database provider.</summary>
        IProvider Provider { get; }


        /// <summary>Gets or sets the current transaction.</summary>
        IDbTransaction Current { get; set; }


        /// <summary>Gets or sets a value indicating if an operation will use the current transaction if it exists and no transaction has been specified.</summary>
        bool UseCurrentTransaction { get; set; }


        /// <summary>Gets an array containing all current transactions.</summary>
        IDbTransaction[] All { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Begins a new transaction.</summary>
        /// <param name="makeCurrent">Determines if the result transaction will be the current transaction for this provider.</param>
        /// <returns>Transaction.</returns>
        IDbTransaction Begin(bool makeCurrent = true);


        /// <summary>Begins a new transaction.</summary>
        /// <param name="il">Isolation level.</param>
        /// <param name="makeCurrent">Determines if the result transaction will be the current transaction for this provider.</param>
        /// <returns>Transaction.</returns>
        IDbTransaction Begin(IsolationLevel il, bool makeCurrent = true);


        /// <summary>Commits the current transaction.</summary>
        void Commit();


        /// <summary>Commits all current transactions.</summary>
        void CommitAll();


        /// <summary>Rolls back the current transaction.</summary>
        void Rollback();


        /// <summary>Rolls back all current transactions.</summary>
        void RollbackAll();
    }
}
