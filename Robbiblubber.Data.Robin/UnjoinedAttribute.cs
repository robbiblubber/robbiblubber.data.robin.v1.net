﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a database-bound class as not joined to a superclass.</summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class UnjoinedAttribute: Attribute
    {}
}
