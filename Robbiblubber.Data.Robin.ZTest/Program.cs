﻿using System;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Data.Providers.SQLite;



namespace Robbiblubber.Data.Robin.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager.Provider = SQLiteProvider.ForFile(@"C:\home\projects\Robbiblubber.Data.Robin.NET\res\test_Robin.sqlite");
            Manager.Provider.Connect();

            foreach(var i in Manager.Get<Vehicle>().OrderBy(m => m.Weight))
            {
                Console.Write(i.ID + "   " + i.Weight.ToString().PadRight(8));

                if(i is Car) { Console.WriteLine("Car:    " + ((Car) i).Motors.ToString() + " Motors."); }
                else if(i is Chaise) { Console.WriteLine("Chaise: " + ((Chaise) i).Horses.ToString() + " Horses."); }
                else if(i is Oxcart) { Console.WriteLine("Oxcart: " + ((Oxcart) i).Oxes.ToString() + " Oxes."); }
                else { Console.WriteLine("Other vehicle."); }
            }

            Console.WriteLine("\r\nReady.");
            Console.ReadLine();
        }

        public static IQueryable<Student> GetFem()
        {
            return Manager.Get<Student>(m => m.Gender == Gender.FEMALE);
        }
    }


    public class GenderEq: IEqualityComparer<Gender>
    {
        public bool Equals(Gender x, Gender y)
        {
            return x == y;
        }

        public int GetHashCode(Gender obj)
        {
            return obj.GetHashCode();
        }
    }


    public class StudentEq: IEqualityComparer<Student>
    {
        public bool Equals(Student x, Student y)
        {
            if(x == null) { return (y == null); }

            return x.Equals(y);
        }


        public int GetHashCode(Student obj)
        {
            return obj.GetHashCode();
        }
    }
}
