﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements a mutable list.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class MutableQueryableList<T>: ExtensibleQueryableList<T>, IMutableList<T>, IExtensibleList<T>, IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MutableQueryableList(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="items">Items.</param>
        public MutableQueryableList(IEnumerable items): base(items)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMutableList<T>                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        public virtual new T this[int i]
        {
            get { return base[i]; }
            set { _Items[i] = value; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>Clears the list.</summary>
        public virtual void Clear()
        {
            _Items.Clear();
        }


        /// <summary>Removes an item.</summary>
        /// <param name="item">Item.</param>
        public virtual bool Remove(T item)
        {
            return _Items.Remove(item);
        }


        /// <summary>Removes the item with the given index.</summary>
        /// <param name="i">Index.</param>
        public virtual void RemoveAt(int i)
        {
            _Items.RemoveAt(i);
        }


        /// <summary>Copies the list to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Start index.</param>
        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            _Items.CopyTo(array, arrayIndex);
        }
    }
}
