﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Data.Robin.Base;
using Robbiblubber.Util.Collections;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This class provides an extensible object list.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class ExtensibleObjectList<T>: ImmutableObjectList<T>, IExtensibleObjectList<T>, IExtensibleList<T>, IImmutableObjectList<T>, IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IEnumerable, __IObjectList
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Virgin flag.</summary>
        protected bool _Virgin = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="query">SQL query.</param>
        protected internal ExtensibleObjectList(ISQLQuery query): base(query)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        protected internal ExtensibleObjectList(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="list"></param>
        /// <param name="target"></param>
        protected internal ExtensibleObjectList(object list, object target)
        {
            _List = list;
            _Target = target;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExtensibleList<T>                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        public virtual void Add(T item)
        {
            _Load();

            if(_Items.Contains(item)) return;

            _Virgin = false;
            _Items.Add(item);

            if(_List != null) { ((__ListDescriptor) _List).Propagate(false, item, _Target); }
        }


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        public virtual void AddRange(IEnumerable<T> items)
        {
            foreach(T i in items) { Add(i); }
        }


        /// <summary>Inserts an item at a given position.</summary>
        /// <param name="i">Index.</param>
        /// <param name="item">Item.</param>
        public virtual void Insert(int i, T item)
        {
            _Load();
            _Virgin = false;
            _Items.Insert(i, item);

            if(_List != null) { ((__ListDescriptor) _List).Propagate(false, item, _Target); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] ImmutableObjectList<T>                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the list.</summary>
        public override void Refresh()
        {
            _Virgin = true;
            _Items = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IObjectList                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object virgin flag.</summary>
        bool __IObjectList.Virgin
        {
            get { return _Virgin; }
        }


        /// <summary>Adds an object to the list.</summary>
        /// <param name="obj">Object.</param>
        void __IObjectList.Add(object obj)
        {
            if(_Items != null) Add((T) obj);
        }


        /// <summary>Removes an object from the list.</summary>
        /// <param name="obj">Object.</param>
        void __IObjectList.Remove(object obj)
        { 
            if(_Items != null)
            {
                if(_Items.Contains((T) obj)) { _Items.Remove((T) obj); }
            }
        }
    }
}
