﻿using System;
using System.Reflection;

using Robbiblubber.Data.Robin.Base;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a database-bound field.</summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class FieldAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The database column name.</summary>
        public string ColumnName = null;

        /// <summary>The database column alias.</summary>
        public string ColumnAlias = null;

        /// <summary>The database data type.</summary>
        public DataType DataType = DataType.AUTO;

        /// <summary>Data size.</summary>
        public int Size = 0;

        /// <summary>Always write flag.</summary>
        public bool AlwaysWrite = false;

        /// <summary>Marshal method class name.</summary>
        public string MarshalClass = null;

        /// <summary>Marshal method name.</summary>
        /// <remarks>Marshal methods must be static.</remarks>
        public string MarshalMethod = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="columnAlias">Column alias.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="size">Data size.</param>
        public FieldAttribute(string columnName = null, string columnAlias = null, DataType dataType = DataType.AUTO, int size = 0)
        {
            ColumnName = columnName;
            ColumnAlias = columnAlias;
            DataType = dataType;
            Size = size;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQL data type for this field.</summary>
        public SQLDataType SqlDataType
        {
            get
            {
                switch(DataType)
                {
                    case DataType.STRING: return SQLDataType.String(Size);
                    case DataType.BOOLEAN: return SQLDataType.BOOLEAN;
                    case DataType.BOOLEAN_INTEGER: return SQLDataType.BOOLEAN_INTEGER;
                    case DataType.TIMESTAMP: return SQLDataType.TIMESTAMP;
                    case DataType.INT16: return SQLDataType.INT16;
                    case DataType.INT32: return SQLDataType.INT32;
                    case DataType.INT64: return SQLDataType.INT64;
                }

                return null;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the primary key generator delegate.</summary>
        /// <param name="t">Type.</param>
        public MarshalField GetMarshalMethod(Type t)
        {
            if(string.IsNullOrWhiteSpace(MarshalMethod)) return null;

            try
            {
                Type gen = null;
                try
                {
                    if(!string.IsNullOrWhiteSpace(MarshalClass)) { gen = Type.GetType(MarshalClass); }
                }
                catch(Exception) {}

                if(gen == null) { gen = t; }
                MethodInfo m = gen.GetMethod(MarshalMethod, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);

                if(m != null) return (MarshalField) Delegate.CreateDelegate(typeof(MarshalField), m);
            }
            catch(Exception) {}

            return null;
        }
    }



    /// <summary>This enumeration defines valid data types.</summary>
    public enum DataType: int
    {
        /// <summary>Automatically detect data type.</summary>
        AUTO = 0,
        /// <summary>String data type.</summary>
        STRING = 1,
        /// <summary>Boolean data type.</summary>
        BOOLEAN = 4,
        /// <summary>Boolean data type represented as integer.</summary>
        BOOLEAN_INTEGER = 5,
        /// <summary>Date/time data type.</summary>
        TIMESTAMP = 6,
        /// <summary>16 bit integer data type.</summary>
        INT16 = 7,
        /// <summary>32 bit integer data type.</summary>
        INT32 = 8,
        /// <summary>64 bit integer data type.</summary>
        INT64 = 9
    }
}
