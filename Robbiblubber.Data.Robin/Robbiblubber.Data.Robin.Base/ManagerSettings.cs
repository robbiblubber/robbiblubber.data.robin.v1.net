﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class defines settings for the framework manager.</summary>
    public sealed class ManagerSettings
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if unmarked public fields will be treated as database-bound.</summary>
        public bool RegisterUnmarkedFields
        {
            get; set;
        } = false;


        /// <summary>Gets or sets if unmarked public properties will be treated as database-bound.</summary>
        public bool RegisterUnmarkedProperties
        {
            get; set;
        } = false;


        /// <summary>Gets or sets if database names will always be used uppercase.</summary>
        public bool DatabaseNamesUppercase
        {
            get; set;
        } = true;
    }
}
