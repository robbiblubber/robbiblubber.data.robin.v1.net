﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Data.Providers;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>Entity classes that are used with the expression parser need to implement this interface.</summary>
    public interface IParsableEntity
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the database provider for this entity.</summary>
        IProvider Provider { get; }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a query for this instance.</summary>
        /// <returns>SQL query.</returns>
        ISQLQuery CreateQuery();


        /// <summary>Gets a field by its name.</summary>
        /// <param name="name">Field name.</param>
        /// <returns></returns>
        IParsableField GetField(string name);
    }
}
