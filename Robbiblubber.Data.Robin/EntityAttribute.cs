﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a class as a database-bound entity.</summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class EntityAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="tableAlias">Table alias.</param>
        /// <param name="whereClause">Where clause.</param>
        public EntityAttribute(string tableName = null, string tableAlias = null, string whereClause = null)
        {
            TableName = tableName;
            TableAlias = tableAlias;
            WhereClause = whereClause;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The database table name.</summary>
        public string TableName = null;


        /// <summary>The database table alias.</summary>
        public string TableAlias = null;


        /// <summary>Default where clause.</summary>
        public string WhereClause = null;
    }
}
