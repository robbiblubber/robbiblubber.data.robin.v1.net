﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements a list of field descriptors.</summary>
    internal sealed class __ListDescriptorList: IEnumerable<__ListDescriptor>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item list.</summary>
        private LinkedList<__ListDescriptor> _Items = new LinkedList<__ListDescriptor>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns the added item.</returns>
        public __ListDescriptor Add(__ListDescriptor item)
        {
            _Items.AddLast(item);
            return item;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<__FieldDescriptor>                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<__ListDescriptor> IEnumerable<__ListDescriptor>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }
    }
}
