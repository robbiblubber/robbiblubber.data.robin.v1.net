﻿using System;
using System.Data;



namespace Robbiblubber.Data.Robin.Transactions
{
    /// <summary>This class implements a transaction.</summary>
    public class Transaction: IDbTransaction, IDisposable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent transaction provider.</summary>
        protected TransactionProvider _Parent;

        /// <summary>Database transaction.</summary>
        protected IDbTransaction _T;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent transaction provider.</param>
        /// <param name="il">Isolation level.</param>
        protected internal Transaction(TransactionProvider parent, IsolationLevel il)
        {
            _Parent = parent;
            _T = _Parent.Provider.Connection.BeginTransaction(il);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Removes the current transaction from the parent transaction provider.</summary>
        protected void _Remove()
        {
            if(_Parent.Current == this) { _Parent.Current = null; }
            try
            {
                _Parent._All.Remove(this);
            }
            catch(Exception) {}

            _T.Dispose();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITransactionProvider                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the connection associated with this transaction.</summary>
        IDbConnection IDbTransaction.Connection
        {
            get { return _T.Connection; }
        }


        /// <summary>Gets the transaction isolation level.</summary>
        IsolationLevel IDbTransaction.IsolationLevel
        {
            get { return _T.IsolationLevel; }
        }


        /// <summary>Commits the transaction.</summary>
        public virtual void Commit()
        {
            _T.Commit();
            _Remove();
        }

        
        /// <summary>Rolls back the transaction.</summary>
        public void Rollback()
        {
            _T.Rollback();
            _Remove();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        void IDisposable.Dispose()
        {
            try
            {
                _T.Dispose();
            }
            catch(ObjectDisposedException) {}
        }
    }
}
