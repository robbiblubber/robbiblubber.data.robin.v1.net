﻿using System;



namespace Robbiblubber.Data.Robin.Test
{
    [Entity(TableName = "CLASSES")]
    public class Class: Atom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Class(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the time of the last change to the object.</summary>
        [ChangeTime(ColumnName = "JTIME")]
        protected DateTime TimeChanged { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the class ID.</summary>
        [Primary(Size = 24)]
        public string ID { get; protected set; }


        /// <summary>Gets or sets the class name.</summary>
        [Field(Size = 64)]
        public string Name { get; set; }


        /// <summary>Gets or sets the class teacher.</summary>
        [Foreign(ColumnName = "KTEACHER")]
        public Teacher Teacher { get; set; }


        /// <summary>Gets the students in this class.</summary>
        [List1ToN]
        public IExtensibleObjectList<Student> Students { get; protected set; }
    }
}
