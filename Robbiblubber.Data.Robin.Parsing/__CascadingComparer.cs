﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements a cascading provider.</summary>
    internal sealed class __CascadingComparer: IComparer<object>, IComparer
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Comparers.</summary>
        private IEnumerable<IComparer> _Comparers;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="comparers">Comparers.</param>
        public __CascadingComparer(IEnumerable<IComparer> comparers)
        {
            _Comparers = comparers;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two instances.</summary>
        /// <param name="x">Object being compared.</param>
        /// <param name="y">Object comparing to.</param>
        /// <returns>Result.</returns>
        public int Compare(object x, object y)
        {
            foreach(IComparer i in _Comparers)
            {
                if(i.Compare(x, y) != 0) { return i.Compare(x, y); }
            }

            return 0;
        }
    }
}
