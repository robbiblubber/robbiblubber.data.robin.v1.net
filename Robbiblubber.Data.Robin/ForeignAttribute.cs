﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a database-bound primary key field.</summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class ForeignAttribute: FieldAttribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="columnAlias">Column alias.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="size">Data size.</param>
        public ForeignAttribute(string columnName = null, string columnAlias = null, DataType dataType = DataType.AUTO, int size = 0): base(columnName, columnAlias, dataType, size)
        {}
    }
}
