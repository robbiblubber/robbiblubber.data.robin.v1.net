﻿using System;
using System.Data;
using System.Reflection;

using Robbiblubber.Data.Robin.Parsing;
using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class provides field information.</summary>
    internal sealed class __FieldDescriptor: __FieldDescriptorBase, IParsableField, ISQLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Field number.</summary>
        internal int _N = 0;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <param name="member">Member.</param>
        public __FieldDescriptor(__EntityDescriptor entity, MemberInfo member): base(entity, member)
        {
            FieldAttribute a = null;
            if(member.GetCustomAttribute<PrimaryAttribute>() != null)
            {
                a = member.GetCustomAttribute<PrimaryAttribute>();
                IsPrimaryKey = true;
                IsAutoIncrement = ((PrimaryAttribute) a).AutoIncrement;
                entity.PrimaryKey = this;
                entity._NextPrimaryGenerator = ((PrimaryAttribute) a).GetGenerator(member.ReflectedType);
            }

            if(member.GetCustomAttribute<ForeignAttribute>() != null)
            {
                if(a == null) { a = member.GetCustomAttribute<ForeignAttribute>(); }
                IsForeignKey = true;
                TargetEntity = Manager._EntityCache[FieldType];
            }

            if(member.GetCustomAttribute<ChangeTimeAttribute>() != null)
            {
                if(a == null) { a = member.GetCustomAttribute<ChangeTimeAttribute>(); }
                IsChangeTime = true;
                Entity.ChangeTime = this;
            }

            if(a == null) { a = member.GetCustomAttribute<FieldAttribute>(); }
            
            ColumnName  = (((a == null) || a.ColumnName  == null) ? member.Name : a.ColumnName);
            ColumnAlias = (((a == null) || a.ColumnAlias == null) ? ColumnName  : a.ColumnAlias);

            if(Manager.Settings.DatabaseNamesUppercase) { ColumnName = ColumnName.ToUpper(); }

            Size = ((a == null) ? 0 : a.Size);
            DataType = (((a == null) || a.DataType == Robin.DataType.AUTO) ? _GuessDataType() : a.SqlDataType);

            AlwaysWrite = ((a == null) ? false : a.AlwaysWrite);
            if(a != null) { _Marshal = a.GetMarshalMethod(entity.Type); }

            if(IsPrimaryKey) { entity.PrimaryKey = this; }
            if(IsForeignKey) { TargetEntity = Manager._EntityCache[FieldType]; }

            if(Entity.IsChild)
            {
                if(member.DeclaringType != Entity.Type) { Inherited = true; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static methods                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the field attribute for a member.</summary>
        /// <param name="member">Member.</param>
        /// <returns>Field attribute.</returns>
        internal static Attribute _GetFieldAttribute(MemberInfo member)
        {
            FieldAttribute  a = member.GetCustomAttribute<PrimaryAttribute>();
            if(a == null) { a = member.GetCustomAttribute<ForeignAttribute>(); } else { return a; }
            if(a == null) { a = member.GetCustomAttribute<ChangeTimeAttribute>(); } else { return a; }
            if(a == null) { a = member.GetCustomAttribute<FieldAttribute>(); }

            return a;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Evaluates the data type from field type.</summary>
        /// <returns>Database data type.</returns>
        private SQLDataType _GuessDataType()
        {
            if(FieldType == typeof(short))    { return SQLDataType.INT16; }
            if(FieldType == typeof(int))      { return SQLDataType.INT32; }
            if(FieldType == typeof(long))     { return SQLDataType.INT64; }
            if(FieldType == typeof(byte))     { return SQLDataType.INT16; }
            if(FieldType == typeof(string))   { return SQLDataType.String(Size); }
            if(FieldType == typeof(DateTime)) { return SQLDataType.TIMESTAMP; }
            if(FieldType == typeof(bool))     { return SQLDataType.BOOLEAN; }
            if(FieldType == typeof(float))    { return SQLDataType.FLOAT; }
            if(FieldType == typeof(double))   { return SQLDataType.DOUBLE; }
            if(FieldType == typeof(decimal))  { return SQLDataType.DECIMAL; }

            if(FieldType.IsEnum) { return SQLDataType.INTEGER; }

            if(Manager._EntityCache[FieldType] != null)
            {
                IsForeignKey = true;
                return Manager._EntityCache[FieldType].PrimaryKey.DataType;
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the field name.</summary>
        public string Name
        {
            get { return Member.Name; }
        }


        /// <summary>Gets the data size.</summary>
        public int Size
        {
            get; private set;
        }


        /// <summary>Gets the field type.</summary>
        public Type FieldType
        {
            get
            {
                if(Member is PropertyInfo) { return ((PropertyInfo) Member).PropertyType; }
                return ((FieldInfo) Member).FieldType;
            }
        }


        /// <summary>Gets the foreign key target entity.</summary>
        public __EntityDescriptor TargetEntity
        {
            get; private set;
        }


        /// <summary>Gets if the field is inherited from a database-bound superclass.</summary>
        public bool Inherited
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field is the primary key.</summary>
        public bool IsPrimaryKey
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field is auto-increment.</summary>
        public bool IsAutoIncrement
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field cannot be null.</summary>
        public bool IsNotNull
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field is indexed.</summary>
        public bool IsIndexed
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field is unique.</summary>
        public bool IsUnique
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field is a foreign key.</summary>
        public bool IsForeignKey
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field contains the change timestamp.</summary>
        public bool IsChangeTime
        {
            get; private set;
        } = false;


        /// <summary>Gets if the field will be written regardless of its change state.</summary>
        public bool AlwaysWrite
        {
            get; private set;
        } = false;


        /// <summary>Gets or sets a sort key for the field.</summary>
        public string SortKey
        {
            get
            {
                string m;
                if(IsPrimaryKey) { m = "A"; }
                else if(IsForeignKey) { m = "X"; }
                else if(IsChangeTime) { m= "Y"; }
                else if(DataType.ShortName == "i") { m = "D"; }
                else if(DataType.ShortName.StartsWith("f")) { m = "L"; }
                else if(DataType.ShortName == "s") { m = "G"; }
                else if(DataType.ShortName == "sx") { m = "H"; }
                else { m= "J"; }

                return (m + _N.ToString().PadLeft(3, '0'));
            }
        }


        /// <summary>Gets the full column name including table alias.</summary>
        public string FullColumnName
        {
            get { return TableAlias + "." + ColumnName; }
        }


        /// <summary>Returns if the field exists in an instance.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the field exists, otherwise returns FALSE.</returns>
        public bool FieldExists(object obj)
        {
            if(obj == null) return false;
            return Member.DeclaringType.IsAssignableFrom(obj.GetType());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /// <summary>Sets the field value for an instance.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="re">Data.</param>
        public void SetValue(object obj, IDataReader re)
        {
            if(IsForeignKey)
            {
                SetValue(obj, TargetEntity.Get(DataType.GetValue(re, ColumnName)), false);
            }
            else if(_Marshal != null)
            {
                SetValue(obj, re.GetValue(re.GetOrdinal(ColumnName)), true);
            }
            else if(MemberType.IsEnum)
            {
                SetValue(obj, Enum.ToObject(MemberType, DataType.GetValue(re, ColumnName)), false);
            }
            else
            {
                SetValue(obj, DataType.GetValue(re, ColumnName), false);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IParsableField                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the database data type.</summary>
        public SQLDataType DataType
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISQLColumn                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table alias.</summary>
        public string TableAlias 
        { 
            get { return Entity.TableAlias; }
        }


        /// <summary>Gets the column name.</summary>
        public string ColumnName { get; private set; }


        /// <summary>Gets the column alias.</summary>
        public string ColumnAlias { get; private set; }


        /// <summary>Gets the column expression as required in a where clause.</summary>
        public string ColumnExpression 
        { 
            get 
            { 
                if(Inherited && Entity.IsJoined)
                {
                    return Entity.Super.Fields[Name].ColumnExpression;
                }

                return (Entity.TableAlias + "." + ColumnName); 
            }
        }
    }
}
