﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This enumeration defines Marshal directions.</summary>
    public enum MarshalDirection: int
    {
        /// <summary>IN direction, used to SET the field value from database.</summary>
        IN  = 0,
        /// <summary>OUT direction, used to GET the field value for database.</summary>
        OUT = 1
    }
}
