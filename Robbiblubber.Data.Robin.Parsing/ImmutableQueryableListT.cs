﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements a simple list implementing IQueryable.></summary>
    /// <typeparam name="T">Type.</typeparam>
    public class ImmutableQueryableList<T>: IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IQueryable, IEnumerable, __IList
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Sorting.</summary>
        private List<IComparer> _Sorting = new List<IComparer>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item list.</summary>
        protected internal List<T> _Items = new List<T>();

        /// <summary>Query provider.</summary>
        protected IQueryProvider _Provider = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ImmutableQueryableList()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="items">Items.</param>
        public ImmutableQueryableList(IEnumerable items)
        {
            foreach(T i in items) { _Items.Add(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList<T>                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        public virtual T this[int i] 
        { 
            get { return _Items[i]; }
        }


        /// <summary>Gets the number of items in this collection.</summary>
        public virtual int Count 
        { 
            get { return _Items.Count; }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public virtual bool Contains(T item)
        {
            return _Items.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        public virtual int IndexOf(T item)
        {
            return _Items.IndexOf(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IList                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the underlying list for this instance.</summary>
        /// <returns>List.</returns>
        IList __IList.GetList()
        {
            return _Items;
        }


        /// <summary>Sets the underlying list for this instance.</summary>
        /// <param name="list">List.</param>
        void __IList.SetList(IList list)
        {
            _Items = (List<T>) list;
        }


        /// <summary>Gets the sorting for this instance.</summary>
        /// <returns>Sorting.</returns>
        List<IComparer> __IList.GetSorting()
        {
            return _Sorting;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IQueryable                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the expression tree associated with this instance.</summary>
        Expression IQueryable.Expression
        {
            get { return Expression.Constant(this); }
        }

        /// <summary>Gets the type of elements that is returned when the expression tree is executed.</summary>
        Type IQueryable.ElementType
        {
            get { return typeof(T); }
        }

        /// <summary>Gets the query provider that is associated with this data source.</summary>
        IQueryProvider IQueryable.Provider
        {
            get
            {
                if(_Provider == null) { _Provider = new QueryProvider(this); }

                return _Provider;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator,</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this list.</summary>
        /// <returns>Enumerator,</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }
    }
}
