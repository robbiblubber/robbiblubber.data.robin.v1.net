﻿using System;
using System.Collections;
using System.Linq.Expressions;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class implements an expression-based comparer.</summary>
    internal sealed class __ExpressionComparer: IComparer
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Comparison delegate.</summary>
        private Delegate _Comp;

        /// <summary>Descending flag.</summary>
        private bool _Desc;
        
        /// <summary>Comparer.</summary>
        private IComparer _Comparer = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="desc">Determies if the search is descending.</param>
        public __ExpressionComparer(LambdaExpression expression, bool desc = false)
        {
            _Comp = expression.Compile();
            _Desc = desc;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="comparer">Comparer.</param>
        /// <param name="desc">Determies if the search is descending.</param>
        public __ExpressionComparer(LambdaExpression expression, IComparer comparer, bool desc = false)
        {
            _Comp = expression.Compile();
            _Desc = desc;
            _Comparer = comparer;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two instances.</summary>
        /// <param name="x">Object being compared.</param>
        /// <param name="y">Object comparing to.</param>
        /// <returns>Result.</returns>
        private int _Compare(object x, object y)
        {
            if(x == null) { return ((y == null) ? 0 : 1); }
            if(y == null) { return -1; }

            x = _Comp.DynamicInvoke(x);
            y = _Comp.DynamicInvoke(y);

            if(_Comparer != null) return _Comparer.Compare(x, y);

            if(x == null) { return ((y == null) ? 0 : 1); }
            if(y == null) { return -1; }

            if(x == null) { return ((y == null) ? 0 : 1); }
            
            if(x is IComparable) { return ((IComparable) x).CompareTo(y); }
            return 0;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparer                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two instances.</summary>
        /// <param name="x">Object being compared.</param>
        /// <param name="y">Object comparing to.</param>
        /// <returns>Result.</returns>
        public int Compare(object x, object y)
        {
            return (_Desc ? -_Compare(x, y) : _Compare(x, y));
        }
    }
}
