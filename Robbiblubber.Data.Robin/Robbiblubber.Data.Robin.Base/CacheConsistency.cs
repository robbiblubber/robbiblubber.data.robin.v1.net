﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This enumeration defines consistency option for a cache.</summary>
    public enum CacheConsistency: int
    {
        /// <summary>Uses default cache consitency options.</summary>
        DEFAULT = 0,
        /// <summary>Provides no automatic cache consistency.</summary>
        NONE = 1,
        /// <summary>Uses time-based cache consistency options.</summary>
        TIMED = 2,
        /// <summary>Provides full automatic cache consistency.</summary>
        FULL = 3
    }
}
