﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements a database-bound lock provider on process level.</summary>
    public class DummyLockProvider: ILockProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public DummyLockProvider()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILockProvider                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a lock on an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the object has been successfully locked, otherwise returns FALSE.</returns>
        public virtual bool CreateLock(object obj)
        {
            return true;
        }


        /// <summary>Creates a lock on an object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the lock has has been successfully removed from the object, otherwise returns FALSE.</returns>
        public virtual bool ReleaseLock(object obj)
        {
            return true;
        }


        /// <summary>Returns if the object is currently locked by this or another context.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="owned">Determines if the call returns TRUE for the calling context.</param>
        /// <returns>Returns TRUE if the object is locked, otherwise returns FALSE.</returns>
        public virtual bool IsLocked(object obj, bool owned = true)
        {
            return owned;
        }


        /// <summary>Returns if the object is currently locked by the calling context.</summary>
        /// <param name="obj">Object intance.</param>
        /// <returns>Returns TRUE if the object is locked by the calling context, otherwise returns FALSE.</returns>
        public virtual bool IsOwned(object obj)
        {
            return true;
        }


        /// <summary>Returns if the object is not locked or locked by the calling context.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the object not locked or locked by the calling context, otherwise returns FALSE.</returns>
        public virtual bool CanLock(object obj)
        {
            return true;
        }
    }
}
