﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.Providers;



namespace Robbiblubber.Data.Robin.Transactions
{
    /// <summary>This class implements the default transaction provider.</summary>
    public class TransactionProvider: ITransactionProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List of all current transactions.</summary>
        protected internal List<IDbTransaction> _All = new List<IDbTransaction>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITransactionProvider                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database provider.</summary>
        public virtual IProvider Provider { get; protected set; }

        
        /// <summary>Gets the current transaction.</summary>
        public virtual IDbTransaction Current { get; set; }


        /// <summary>Gets a value indicating if an operation will use the current transaction if it exists and no transaction has been specified.</summary>
        public virtual bool UseCurrentTransaction
        {
            get; set;
        } = true;


        /// <summary>Gets an array containing all current transactions.</summary>
        public virtual IDbTransaction[] All 
        { 
            get { return _All.ToArray(); }
        }


        /// <summary>Begins a new transaction.</summary>
        /// <param name="makeCurrent">Determines if the result transaction will be the current transaction for this provider.</param>
        /// <returns>Transaction.</returns>
        public virtual IDbTransaction Begin(bool makeCurrent = true)
        {
            return Begin(IsolationLevel.Unspecified, makeCurrent);
        }


        /// <summary>Begins a new transaction.</summary>
        /// <param name="il">Isolation level.</param>
        /// <param name="makeCurrent">Determines if the result transaction will be the current transaction for this provider.</param>
        /// <returns>Transaction.</returns>
        public virtual IDbTransaction Begin(IsolationLevel il, bool makeCurrent = true)
        {
            Transaction rval = new Transaction(this, il);
            if(makeCurrent) Current = rval;

            return rval;
        }


        /// <summary>Commits the current transaction.</summary>
        public virtual void Commit()
        {
            if(Current != null) { Current.Commit(); }
        }


        /// <summary>Commits all current transactions.</summary>
        public virtual void CommitAll()
        {
            foreach(IDbTransaction i in new List<IDbTransaction>(_All)) { i.Commit(); }
        }


        /// <summary>Rolls back the current transaction.</summary>
        public virtual void Rollback()
        {
            if(Current != null) { Current.Rollback(); }
        }


        /// <summary>Rolls back all current transactions.</summary>
        public virtual void RollbackAll()
        {
            foreach(IDbTransaction i in new List<IDbTransaction>(_All)) { i.Rollback(); }
        }
    }
}
