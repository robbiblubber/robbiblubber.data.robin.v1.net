﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>Field classes that are used with the expression parser need to implement this interface.</summary>
    public interface IParsableField: ISQLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the SQL data type for this field.</summary>
        SQLDataType DataType { get; }
    }
}
