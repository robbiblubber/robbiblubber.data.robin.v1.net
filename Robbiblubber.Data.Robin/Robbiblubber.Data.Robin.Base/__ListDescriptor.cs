﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class provides list information.</summary>
    internal abstract class __ListDescriptor: __FieldDescriptorBase
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Target entity.</summary>
        private __EntityDescriptor _TargetEntity = null;

        /// <summary>Preferred type.</summary>
        private Type _PreferredType = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <param name="member">Member.</param>
        protected __ListDescriptor(__EntityDescriptor entity, MemberInfo member): base(entity, member)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the list target type.</summary>
        /// <returns>Target type.</returns>
        public Type TargetType
        {
            get
            {
                if(MemberType.GetInterface("System.Collections.IEnumerable") == null) { return null; }
                if(MemberType.GenericTypeArguments.Length != 1) { return null; }

                if(Manager._EntityCache[MemberType.GenericTypeArguments[0]] != null) { return MemberType.GenericTypeArguments[0]; }
                return null;
            }
        }


        /// <summary>Gets the actual list type.</summary>
        /// <returns>List type.</returns>
        public Type ListType
        {
            get
            {
                if(UsingFrameworkTypes) return PreferredType;
                return MemberType;
            }
        }


        /// <summary>Gets the target entity.</summary>
        public __EntityDescriptor TargetEntity
        {
            get
            {
                if(_TargetEntity == null) { _TargetEntity = Manager._EntityCache[TargetType]; }
                return _TargetEntity;
            }
        }


        /// <summary>Gets the preferred list type.</summary>
        public Type PreferredType
        {
            get
            {
                if(_PreferredType == null) { _PreferredType = ((this is __1ToNDescriptor) ? typeof(ExtensibleObjectList<>) : typeof(MutableObjectList<>)).MakeGenericType(TargetType); }
                return _PreferredType;
            }
        }


        /// <summary>Gets a value indicating if the list is using framework types.</summary>
        public bool UsingFrameworkTypes
        {
            get { return MemberType.IsAssignableFrom(PreferredType); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Initializes the list for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="force">Determines if the object needs to be re-initialized.</param>
        /// <param name="recurse">Determines if the method is called recursively.</param>
        public virtual void Initialize(object obj, bool force, bool recurse = false)
        {
            if(UsingFrameworkTypes)
            {
                if((!force) && (GetValue(obj, false) != null)) return;

                ConstructorInfo c = null;
                foreach(ConstructorInfo i in PreferredType.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if((i.GetParameters().Length == 2) && (i.GetParameters()[0].ParameterType == typeof(object))  && (i.GetParameters()[1].ParameterType == typeof(object)))
                    {
                        c = i; break;
                    }
                }

                SetValue(obj, c.Invoke(new object[] { this, obj }), false);
            }
            else
            {
                IList list = (IList) (MemberType.IsAbstract ? Activator.CreateInstance(typeof(List<>).MakeGenericType(TargetType)) : Activator.CreateInstance(MemberType));

                if(Entity.PrimaryKey.GetValue(obj, false) != null)
                {
                    TargetEntity.Populate(list, GetQuery(obj));
                }
            }
        }


        /// <summary>Gets the SQL query for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>SQL query.</returns>
        public abstract ISQLQuery GetQuery(object obj);


        /// <summary>Saves the list.</summary>
        /// <param name="obj">Object.</param>
        public abstract void Save(object obj);


        /// <summary>Adds an item to the far end list of an m:n relation if necessary.</summary>
        /// <param name="remove">Determines if the item is added (FALSE) or removed (TRUE).</param>
        /// <param name="target">Target object.</param>
        /// <param name="obj">Object.</param>
        public virtual void Propagate(bool remove, object target, object obj)
        {}
    }
}
