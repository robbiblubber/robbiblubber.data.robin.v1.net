﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>Creates the next primary key for a type.</summary>
    /// <param name="t">Type.</param>
    /// <returns>Next value.</returns>
    public delegate object CreateNextPrimary(Type t);
}
