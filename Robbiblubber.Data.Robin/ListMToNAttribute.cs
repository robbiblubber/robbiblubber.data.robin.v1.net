﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a list as part of a database-bound m:n relation.</summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class ListMToNAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="relationTableName">Relation table name.</param>
        /// <param name="nearForeignKey">Near foreign key column name.</param>
        /// <param name="farForeignKey">Far foreign key column name.</param>
        public ListMToNAttribute(string relationTableName = null, string nearForeignKey = null, string farForeignKey = null)
        {
            RelationTableName = relationTableName;
            NearForeignKey = nearForeignKey;
            FarForeignKey = farForeignKey;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the relation table name.</summary>
        public string RelationTableName = null;

        /// <summary>Gets the near foreign key column name.</summary>
        /// <remarks>The near foreign key is the foreign key that refers to the entity that contains the field marked with this attribute.</remarks>
        public string NearForeignKey = null;

        /// <summary>Gets the far foreign key column name.</summary>
        /// <remarks>The far foreign key is the foreign key that refers to the entity that does not contain the field marked with this attribute.</remarks>
        public string FarForeignKey = null;
    }
}
