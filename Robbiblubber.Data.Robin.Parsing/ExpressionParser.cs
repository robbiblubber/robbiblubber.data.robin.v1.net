﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

using Robbiblubber.Data.SQL;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>This class provides expression parsing.</summary>
    public class ExpressionParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Parent entity.</param>
        public ExpressionParser(IParsableEntity entity)
        {
            Entity = entity;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the sorting for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Sorting.</returns>
        private static List<IComparer> GetSorting(object obj)
        {
            if(obj is __IList) { return ((__IList) obj).GetSorting(); }

            try
            {
                foreach(FieldInfo i in obj.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if(i.Name == "_Sorting") { return (List<IComparer>) i.GetValue(obj); }
                }
            } catch(Exception) {}

            return new List<IComparer>();
        }


        /// <summary>Converts a return value to a given (numeric) type.</summary>
        /// <param name="value">Value.</param>
        /// <param name="resultType">Type.</param>
        /// <returns>Value converted to result type.</returns>
        private static object _ToResultType(object value, Type resultType)
        {
            if(resultType == typeof(int?))
            {
                if(value == null) return null;
                return Convert.ToInt32(value);
            }
            if(resultType == typeof(int))
            {
                return ((value == null) ? 0 : Convert.ToInt32(value));
            }

            if(resultType == typeof(long?))
            {
                if(value == null) return null;
                return Convert.ToInt64(value);
            }
            if(resultType == typeof(double))
            {
                return ((value == null) ? 0 : Convert.ToInt64(value));
            }

            if(resultType == typeof(decimal?))
            {
                if(value == null) return null;
                return Convert.ToDecimal(value);
            }
            if(resultType == typeof(decimal))
            {
                return ((value == null) ? 0 : Convert.ToDecimal(value));
            }

            if(resultType == typeof(double?))
            {
                if(value == null) return null;
                return Convert.ToDouble(value);
            }
            if(resultType == typeof(double))
            {
                return ((value == null) ? 0 : Convert.ToDouble(value));
            }

            if(resultType == typeof(float?))
            {
                if(value == null) return null;
                return Convert.ToSingle(value);
            }
            if(resultType == typeof(float))
            {
                return ((value == null) ? 0 : Convert.ToSingle(value));
            }

            return null;
        }


        /// <summary>Performs an ELEMENTAT or ELEMENTATORDEFAULT operation.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="returnDefault">Determines if a default value is returned if no element exists for this query.</param>
        /// <returns>Result.</returns>
        private static object _ElementAt(Expression expression, bool returnDefault)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            int tar = (int) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;
            int n = 0;

            foreach(object i in list)
            {
                if(n++ == tar) { return i; }
            }

            if(returnDefault)
            {
                return (list.GetType().GetGenericArguments()[0].IsValueType ? Activator.CreateInstance(list.GetType().GetGenericArguments()[0]) : null);
            }

            throw new IndexOutOfRangeException();
        }


        /// <summary>Performs an FIRST or FIRSTORDEFAULT operation.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="returnDefault">Determines if a default value is returned if no element exists for this query.</param>
        /// <returns>Result.</returns>
        private static object _First(Expression expression, bool returnDefault)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            if(((MethodCallExpression) expression).Arguments.Count == 1)
            {
                foreach(object i in list) { return i; }
            }
            else
            {
                Delegate filter = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();

                foreach(object i in list)
                {
                    if((bool) filter.DynamicInvoke(i)) { return i; }
                }
            }

            if(returnDefault)
            {
                return (list.GetType().GetGenericArguments()[0].IsValueType ? Activator.CreateInstance(list.GetType().GetGenericArguments()[0]) : null);
            }

            throw new InvalidOperationException("Sequence contains no elements.");
        }


        /// <summary>Performs an LAST or LASTORDEFAULT operation.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="returnDefault">Determines if a default value is returned if no element exists for this query.</param>
        /// <returns>Result.</returns>
        private static object _Last(Expression expression, bool returnDefault)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            object rval = null;
            bool found = false;

            if(((MethodCallExpression) expression).Arguments.Count == 1)
            {
                foreach(object i in list) { rval = i; found = true; }
            }
            else
            {
                Delegate filter = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();

                foreach(object i in list)
                {
                    if((bool) filter.DynamicInvoke(i)) { rval = i; found = true; }
                }
            }

            if(found) { return rval; }

            if(returnDefault)
            {
                return (list.GetType().GetGenericArguments()[0].IsValueType ? Activator.CreateInstance(list.GetType().GetGenericArguments()[0]) : null);
            }

            throw new InvalidOperationException("Sequence contains no elements.");
        }


        /// <summary>Performs a MAX operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="returnDefault">Determines if a default value is returned if no element exists for this query.</param>
        /// <returns>Result.</returns>
        private static object _Max(Expression expression, bool returnDefault)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            object rval = null;
            bool found = false;
            Delegate f = null;

            if(((MethodCallExpression) expression).Arguments.Count == 2)
            {
                f = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            }

            foreach(object i in list)
            {
                if(found)
                {
                    if(f == null)
                    {
                        if(((IComparable) i).CompareTo(rval) > 0) { rval = i; }
                    }
                    else if(((IComparable) f.DynamicInvoke(i)).CompareTo(rval) > 0) { rval = f.DynamicInvoke(i); }
                }
                else { rval = ((f == null) ? i : f.DynamicInvoke(i)); found = true; }
            }

            if(found) { return rval; }

            if(returnDefault)
            {
                return (list.GetType().GetGenericArguments()[0].IsValueType ? Activator.CreateInstance(list.GetType().GetGenericArguments()[0]) : null);
            }

            throw new InvalidOperationException("Sequence contains no elements.");
        }


        /// <summary>Performs a MIN operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="returnDefault">Determines if a default value is returned if no element exists for this query.</param>
        /// <returns>Result.</returns>
        private static object _Min(Expression expression, bool returnDefault)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            object rval = null;
            bool found = false;
            Delegate f = null;

            if(((MethodCallExpression) expression).Arguments.Count == 2)
            {
                f = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            }

            foreach(object i in list)
            {
                if(found)
                {
                    if(f == null)
                    {
                        if(((IComparable) i).CompareTo(rval) < 0) { rval = i; }
                    }
                    else if(((IComparable) f.DynamicInvoke(i)).CompareTo(rval) < 0) { rval = f.DynamicInvoke(i); }
                }
                else { rval = ((f == null) ? i : f.DynamicInvoke(i)); found = true; }
            }

            if(found) { return rval; }

            if(returnDefault)
            {
                return (list.GetType().GetGenericArguments()[0].IsValueType ? Activator.CreateInstance(list.GetType().GetGenericArguments()[0]) : null);
            }

            throw new InvalidOperationException("Sequence contains no elements.");
        }


        /// <summary>Performs an ORDERBY or ORDERBYDESC operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="desc">Determies if the search is descending.</param>
        /// <returns>Selection.</returns>
        private static IQueryable _OrderBy(Expression expression, bool desc)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IQueryable rval = (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));

            IComparer c = ((((MethodCallExpression) expression).Arguments.Count == 2) ? new __ExpressionComparer((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand, desc) :
                          new __ExpressionComparer((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand, (IComparer) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[2]).Value, desc));

            IList l = (IList) Activator.CreateInstance(typeof(List<>).MakeGenericType(list.GetType().GetGenericArguments()[0]), list);
            ArrayList.Adapter(l).Sort(c);
            ((__IList) rval).SetList(l);

            GetSorting(rval).Clear();
            GetSorting(rval).Add(c);

            return rval;
        }


        /// <summary>Performs an THENBY or THENBYDESC operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="desc">Determies if the search is descending.</param>
        /// <returns>Selection.</returns>
        private static IQueryable _ThenBy(Expression expression, bool desc)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IQueryable rval = (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));

            IComparer c = ((((MethodCallExpression) expression).Arguments.Count == 2) ? new __ExpressionComparer((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand, desc) :
                          new __ExpressionComparer((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand, (IComparer) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[2]).Value, desc));

            List<IComparer> sorting = GetSorting(list);
            sorting.Add(c);

            IList l = (IList) Activator.CreateInstance(typeof(List<>).MakeGenericType(list.GetType().GetGenericArguments()[0]), list);
            ArrayList.Adapter(l).Sort(new __CascadingComparer(sorting));
            ((__IList) rval).SetList(l);

            return rval;
        }


        /// <summary>Performs an UNION or UNIONALL operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="all">All flag.</param>
        /// <returns>Selection.</returns>
        private static IQueryable _Union(Expression expression, bool all)
        {
            IEnumerable list1 = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IEnumerable list2 = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;

            List<object> l = new List<object>();

            IEqualityComparer<object> c = ((((MethodCallExpression) expression).Arguments.Count == 3) ? new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[2]).Value) :
                                                                            (IEqualityComparer<object>) new __DefaultObjectEqualityComparer());
            foreach(object i in list1)
            {
                if((!all) && l.Contains(i, c)) continue;
                l.Add(i);
            }
            foreach(object i in list2)
            {
                if((!all) && l.Contains(i, c)) continue;
                l.Add(i);
            }

            return (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list1.GetType().GetGenericArguments()[0]), l);
        }


        /// <summary>Performs a SINGLE or SINGLEORDEFAULT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="returnDefault">Determines if a default value is returned if no element exists for this query.</param>
        /// <returns>Result.</returns>
        private static object _Single(Expression expression, bool returnDefault)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            int n = 0;
            object rval = null;

            foreach(object i in list)
            {
                if(++n > 1) break;

                rval = i;
            }
            if(n == 1) { return rval; }

            if(returnDefault)
            {
                return (list.GetType().GetGenericArguments()[0].IsValueType ? Activator.CreateInstance(list.GetType().GetGenericArguments()[0]) : null);
            }

            if(n == 0)
            {
                throw new InvalidOperationException("Sequence contains no elements.");
            }
            throw new InvalidOperationException("Sequence contains more than one element.");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the expression query type.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Query type.</returns>
        public static QueryType GetQueryType(Expression expression)
        {
            if(expression is MethodCallExpression)
            {
                switch(((MethodCallExpression) expression).Method.Name)
                {
                    case "Where": return QueryType.WHERE;
                    case "OrderBy": return QueryType.ORDERBY;
                    case "Select": return QueryType.SELECT;
                    case "Aggregate": return QueryType.AGGREGATE;
                    case "All": return QueryType.ALL;
                    case "Any": return QueryType.ANY;
                    case "Average": return QueryType.AVERAGE;
                    case "Cast": return QueryType.CAST;
                    case "Concat": return QueryType.CONCAT;
                    case "Count": return QueryType.COUNT;
                    case "DefaultIfEmpty": return QueryType.DEFAULTIFEMPTY;
                    case "Distinct": return QueryType.DISTINCT;
                    case "ElementAt": return QueryType.ELEMENTAT;
                    case "ElementAtOrDefault": return QueryType.ELEMENTATORDEFAULT;
                    case "Except": return QueryType.EXCEPT;
                    case "First": return QueryType.FIRST;
                    case "FirstOrDefault": return QueryType.FIRSTORDEFAULT;
                    case "GroupBy": return QueryType.GROUPBY;
                    case "GroupJoin": return QueryType.GROUPJOIN;
                    case "Intersect": return QueryType.INTERSECT;
                    case "Join": return QueryType.JOIN;
                    case "Last": return QueryType.LAST;
                    case "LastOrDefault": return QueryType.LASTORDEFAULT;
                    case "LongCount": return QueryType.LONGCOUNT;
                    case "Max": return QueryType.MAX;
                    case "Min": return QueryType.MIN;
                    case "OfType": return QueryType.OFTYPE;
                    case "OrderByDescending": return QueryType.ORDERBYDESCENDING;
                    case "Reverse": return QueryType.REVERSE;
                    case "SelectMany": return QueryType.SELECTMANY;
                    case "SequenceEqual": return QueryType.SEQUENCEEQUAL;
                    case "Single": return QueryType.SINGLE;
                    case "SingleOrDefault": return QueryType.SINGLEORDEFAULT;
                    case "Skip": return QueryType.SKIP;
                    case "SkipWhile": return QueryType.SKIPWHILE;
                    case "Sum": return QueryType.SUM;
                    case "Take": return QueryType.TAKE;
                    case "TakeWhile": return QueryType.TAKEWHILE;
                    case "ThenBy": return QueryType.THENBY;
                    case "ThenByDescending": return QueryType.THENBYDESCENDING;
                    case "Union": return QueryType.UNION;
                    case "UnionAll": return QueryType.UNIONALL;
                    case "Zip": return QueryType.ZIP;
                }
            }

            return QueryType.UNKNOWN;
        }


        /// <summary>Applies an acumualtor function over a squence.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Aggregate(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            if(((MethodCallExpression) expression).Arguments.Count == 2)
            {
                Delegate func = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
                object rval = null;

                foreach(object i in list)
                {
                    if(rval == null)
                    {
                        rval = i;
                    }
                    else { rval = func.DynamicInvoke(rval, i); }
                }

                return rval;
            }
            else
            {
                object rval = ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;
                Delegate func = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[2]).Operand).Compile();
                Delegate selector = null;
                if(((MethodCallExpression) expression).Arguments.Count == 4)
                {
                    selector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[3]).Operand).Compile();
                }

                foreach(object i in list)
                {
                    rval = func.DynamicInvoke(rval, i);
                }

                return ((selector == null) ? rval : selector.DynamicInvoke(rval));
            }
        }


        /// <summary>Performs an ALL operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static bool All(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate func = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();

            foreach(object i in list)
            {
                if(!(bool) func.DynamicInvoke(i)) { return false; }
            }

            return true;
        }


        /// <summary>Performs an ANY operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static bool Any(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate func = null;

            if(((MethodCallExpression) expression).Arguments.Count == 2)
            {
                func = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            }

            foreach(object i in list)
            {
                if(func == null) { return true; }
                if((bool) func.DynamicInvoke(i)) { return true; }
            }

            return false;
        }


        /// <summary>Performs an AVERAGE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Average(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate func = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();

            decimal? rval = null;
            int n = 0;

            foreach(object i in list)
            {
                n++;
                object v = func.DynamicInvoke(i);
                if(v != null)
                {
                    if(rval == null) { rval = 0; }
                    rval += Convert.ToDecimal(v);
                }
            }            
            if(rval != null) { rval = (rval / n); }

            return _ToResultType(rval, func.Method.ReturnType);
        }


        /// <summary>Performs an AVERAGE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Sum(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate func = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();

            decimal? rval = null;
            int n = 0;

            foreach(object i in list)
            {
                n++;
                object v = func.DynamicInvoke(i);
                if(v != null)
                {
                    if(rval == null) { rval = 0; }
                    rval += Convert.ToDecimal(v);
                }
            }

            return _ToResultType(rval, func.Method.ReturnType);
        }


        /// <summary>Performs a CAST operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Cast(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            return (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(((MethodCallExpression) expression).Method.ReturnType.GetGenericArguments()[0]), list);
        }


        /// <summary>Performs a CONCAT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Concat(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IEnumerable v = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;
            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]), list);            
            foreach(object i in v) { rval.GetList().Add(i); }
            
            return (IQueryable) rval;
        }


        /// <summary>Performs a COUNT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static int Count(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            int rval = 0;
            foreach(object i in list) { rval++; }

            return rval;
        }


        /// <summary>Performs a LONGCOUNT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static long LongCount(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            long rval = 0;
            foreach(object i in list) { rval++; }

            return rval;
        }


        /// <summary>Performs a DEFAULTIFEMPTY operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable DefaultIfEmpty(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            foreach(object i in list)
            {
                return (IQueryable) list;
            }
            
            object defaultValue;
            if(((MethodCallExpression) expression).Arguments.Count == 1)
            {
                defaultValue = (list.GetType().GetGenericArguments()[0].IsValueType ? Activator.CreateInstance(list.GetType().GetGenericArguments()[0]) : null);
            }
            else { defaultValue = ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value; }

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));
            rval.GetList().Add(defaultValue);

            return (IQueryable) rval;
        }


        /// <summary>Performs a DISTINCT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Distinct(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IEqualityComparer<object> eq;
            List<object> d = new List<object>();

            if(((MethodCallExpression) expression).Arguments.Count == 2)
            {
                eq = new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value);
            }
            else { eq = new __DefaultObjectEqualityComparer(); }

            foreach(object i in list)
            {
                if(!d.Contains(i, eq)) { d.Add(i); }
            }

            return (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]), d);
        }


        /// <summary>Performs an ELEMENTAT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object ElementAt(Expression expression)
        {
            return _ElementAt(expression, false);
        }


        /// <summary>Performs an ELEMENTAT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object ElementAtOrDefault(Expression expression)
        {
            return _ElementAt(expression, true);
        }


        /// <summary>Performs a EXCEPT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Except(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IEqualityComparer<object> eq;
            List<object> d = new List<object>();
            List<object> exc = new List<object>();

            foreach(object i in (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value)
            {
                exc.Add(i);
            }

            if(((MethodCallExpression) expression).Arguments.Count == 3)
            {
                eq = new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[2]).Value);
            }
            else { eq = new __DefaultObjectEqualityComparer(); }

            foreach(object i in list)
            {
                if(!exc.Contains(i, eq)) { d.Add(i); }
            }

            return (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]), d);
        }


        /// <summary>Performs a FIRST operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object First(Expression expression)
        {
            return _First(expression, false);
        }


        /// <summary>Performs a FIRSTORDEFAULT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object FirstOrDefault(Expression expression)
        {
            return _First(expression, true);
        }


        /// <summary>Performs a LAST operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Last(Expression expression)
        {
            return _Last(expression, false);
        }


        /// <summary>Performs a LASTORDEFAULT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object LastOrDefault(Expression expression)
        {
            return _Last(expression, true);
        }


        /// <summary>Performs a GROUPBY operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable GroupBy(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate keySelector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            Delegate elementSelector = null;
            Delegate resultSelector = null;
            IEqualityComparer<object> eq = new __DefaultObjectEqualityComparer();
            
            for(int i = 2; i < ((MethodCallExpression) expression).Arguments.Count; i++)
            {
                if(((MethodCallExpression) expression).Arguments[i].Type.Name.StartsWith("IEqualityComparer"))
                {
                    eq = new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[i]).Value);
                }
                else if(((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[i]).Operand).Parameters.Count == 2)
                {
                    resultSelector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[i]).Operand).Compile();
                }
                else
                {
                    elementSelector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[i]).Operand).Compile();
                }
            }

            Type elementType = ((elementSelector == null) ? list.GetType().GetGenericArguments()[0] : elementSelector.Method.ReturnType);
            __GroupingList groups = new __GroupingList(elementType, eq);
            foreach(object i in list)
            {
                groups[keySelector.DynamicInvoke(i)].GetList().Add(((elementSelector == null) ? i : elementSelector.DynamicInvoke(i)));
            }

            if(resultSelector == null)
            {
                return (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(typeof(IGrouping<,>).MakeGenericType(keySelector.Method.ReturnType, elementType)), groups);
            }

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(resultSelector.Method.ReturnType));
            foreach(__IGrouping i in groups)
            {
                rval.GetList().Add(resultSelector.DynamicInvoke(i.GetKey(), i.GetList()));
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs a GROUPJOIN operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable GroupJoin(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IEnumerable innerList   = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;
            Delegate outerSelector  = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[2]).Operand).Compile();
            Delegate innerSelector  = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[3]).Operand).Compile();
            Delegate resultSelector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[4]).Operand).Compile();
            IEqualityComparer<object> eq  = ((((MethodCallExpression) expression).Arguments.Count == 5) ? new __DefaultObjectEqualityComparer() :
                                             (IEqualityComparer<object>) (new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[5]).Value)));

            __GroupingList innerGroups = new __GroupingList(innerList.GetType().GetGenericArguments()[0], eq);
            foreach(object i in innerList)
            {
                innerGroups[innerSelector.DynamicInvoke(i)].GetList().Add(i);
            }

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(resultSelector.Method.ReturnType));
            foreach(object i in list)
            {
                rval.GetList().Add(resultSelector.DynamicInvoke(i, innerGroups[outerSelector.DynamicInvoke(i)]));
            }
            
            return (IQueryable) rval;
        }


        /// <summary>Performs a INTERSECT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable Intersect(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            List<object> l = new List<object>();
            foreach(object i in (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value)
            {
                l.Add(i);
            }
            IEqualityComparer<object> eq  = ((((MethodCallExpression) expression).Arguments.Count == 2) ? new __DefaultObjectEqualityComparer() :
                                             (IEqualityComparer<object>) (new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[2]).Value)));

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));
            foreach(object i in list)
            {
                if(l.Contains(i, eq)) { rval.GetList().Add(i); }
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs a JOIN operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable Join(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IEnumerable innerList   = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;
            Delegate outerSelector  = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[2]).Operand).Compile();
            Delegate innerSelector  = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[3]).Operand).Compile();
            Delegate resultSelector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[4]).Operand).Compile();
            IEqualityComparer<object> eq  = ((((MethodCallExpression) expression).Arguments.Count == 5) ? new __DefaultObjectEqualityComparer() :
                                             (IEqualityComparer<object>) (new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[5]).Value)));

            __GroupingList innerGroups = new __GroupingList(innerList.GetType().GetGenericArguments()[0], eq);
            foreach(object i in innerList)
            {
                innerGroups[innerSelector.DynamicInvoke(i)].GetList().Add(i);
            }

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(resultSelector.Method.ReturnType));
            foreach(object i in list)
            {
                foreach(object j in innerGroups[outerSelector.DynamicInvoke(i)])
                {
                    rval.GetList().Add(resultSelector.DynamicInvoke(i, j));
                }
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs a MAX operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Max(Expression expression)
        {
            return _Max(expression, false);
        }


        /// <summary>Performs a MAXORDEFAULT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object MaxOrDefault(Expression expression)
        {
            return _Max(expression, true);
        }


        /// <summary>Performs a MIN operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Min(Expression expression)
        {
            return _Min(expression, false);
        }


        /// <summary>Performs a MINORDEFAULT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object MinOrDefault(Expression expression)
        {
            return _Min(expression, true);
        }


        /// <summary>Performs an OFTYPE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable OfType(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Type resultType = expression.Type.GetGenericArguments()[0];
            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(resultType));

            foreach(object i in list)
            {
                if(i.GetType().IsAssignableFrom(resultType)) { rval.GetList().Add(i); }
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs an REVERSE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Reverse(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));
            foreach(object i in list) { rval.GetList().Insert(0, i); }
            
            return (IQueryable) rval;
        }


        /// <summary>Performs a SEQUENCEEQUAL operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static bool SequenceEqual(Expression expression)
        {
            List<object> a = new List<object>();
            List<object> b = new List<object>();
            foreach(object i in (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value) { a.Add(i); }
            foreach(object i in (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value) { b.Add(i); }

            IEqualityComparer<object> eq  = ((((MethodCallExpression) expression).Arguments.Count == 2) ? new __DefaultObjectEqualityComparer() :
                                             (IEqualityComparer<object>) (new __EqualityComparer(((ConstantExpression) ((MethodCallExpression) expression).Arguments[2]).Value)));

            try
            {
                for(int i = 0; i < a.Count; i++)
                {
                    if(!eq.Equals(a[i], b[i])) { return false; }
                }
            } catch(Exception) { return false; }

            return true;
        }


        /// <summary>Performs a SINGLE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Single(Expression expression)
        {
            return _Single(expression, false);
        }


        /// <summary>Performs a SINGLEORDEFAULT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object SingleOrDefault(Expression expression)
        {
            return _Single(expression, true);
        }


        /// <summary>Performs an SKIP operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Skip(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            int n = 0;
            int skip = (int) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));
            foreach(object i in list) 
            {
                if(n++ >= skip) { rval.GetList().Add(i); }
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs an SKIPWHILE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable SkipWhile(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate skip = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            bool withIndex = (skip.Method.GetParameters().Length == 3);
            bool skipping = true;
            int n = 0;

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));
            foreach(object i in list)
            {
                if(skipping)
                {
                    if(skipping = (bool) (withIndex ? skip.DynamicInvoke(i, n++) : skip.DynamicInvoke(i))) continue;
                }
                
                rval.GetList().Add(i);
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs an TAKE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Take(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            int n = 0;
            int skip = (int) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));
            foreach(object i in list)
            {
                if(n++ < skip) { rval.GetList().Add(i); }
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs an TAKEWHILE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable TakeWhile(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate skip = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            bool withIndex = (skip.Method.GetParameters().Length == 3);
            int n = 0;

            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));
            foreach(object i in list)
            {
                if(!(bool) (withIndex ? skip.DynamicInvoke(i, n++) : skip.DynamicInvoke(i))) break;

                rval.GetList().Add(i);
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs an SKIP operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Union(Expression expression)
        {
            return _Union(expression, false);
        }


        /// <summary>Performs an SKIP operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable UnionAll(Expression expression)
        {
            return _Union(expression, true);
        }


        /// <summary>Performs an ZIP operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable Zip(Expression expression)
        {
            IEnumerable list1 = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            IEnumerable list2 = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[1]).Value;
            Delegate merge = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[2]).Operand).Compile();
            __IList rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(merge.Method.ReturnType));

            foreach(object i in list1)
            {
                foreach(object j in list2) { rval.GetList().Add(merge.DynamicInvoke(i, j)); }
            }

            return (IQueryable) rval;
        }


        /// <summary>Performs an SELECTMANY operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result list.</returns>
        public static IQueryable SelectMany(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate collectionSelector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            Delegate resultSelector = null;
            bool withIndex = (collectionSelector.Method.GetParameters().Length == 3);
            int n = 0;

            __IList rval;
            if(((MethodCallExpression) expression).Arguments.Count == 2)
            {
                rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(collectionSelector.Method.ReturnType.GetGenericArguments()[0]));
            }
            else
            {
                resultSelector = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[2]).Operand).Compile();
                rval = (__IList) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(resultSelector.Method.ReturnType));
            }
    
            foreach(object i in list) 
            {
                IEnumerable selection = (IEnumerable) (withIndex ? collectionSelector.DynamicInvoke(i, n) : collectionSelector.DynamicInvoke(i));

                foreach(object j in selection)
                {
                    if(resultSelector == null)
                    {
                        { rval.GetList().Add(j); }
                    }
                    else { rval.GetList().Add(resultSelector.DynamicInvoke(i, j)); }
                }
            }
            
            return (IQueryable) rval;
        }


        /// <summary>Performs a SELECT operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable Select(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate d = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            IQueryable rval = (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(d.Method.ReturnType));

            foreach(object i in list)
            {
                ((__IList) rval).GetList().Add(d.DynamicInvoke(i));
            }

            return rval;
        }


        /// <summary>Performs a WHERE operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable Where(Expression expression)
        {
            IEnumerable list = (IEnumerable) ((ConstantExpression) ((MethodCallExpression) expression).Arguments[0]).Value;
            Delegate d = ((LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand).Compile();
            IQueryable rval = (IQueryable) Activator.CreateInstance(typeof(ImmutableQueryableList<>).MakeGenericType(list.GetType().GetGenericArguments()[0]));

            foreach(object i in list)
            {
                if((bool) d.DynamicInvoke(i))
                {
                    ((__IList) rval).GetList().Add(i);
                }
            }

            return rval;
        }



        /// <summary>Performs an ORDERBY operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable OrderBy(Expression expression)
        {
            return _OrderBy(expression, false);
        }


        /// <summary>Performs an ORDERBYDESCENDING operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable OrderByDescending(Expression expression)
        {
            return _OrderBy(expression, true);
        }


        /// <summary>Performs an ORDERBY operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable ThenBy(Expression expression)
        {
            return _ThenBy(expression, false);
        }


        /// <summary>Performs an ORDERBYDESCENDING operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Selection.</returns>
        public static IQueryable ThenByDescending(Expression expression)
        {
            return _ThenBy(expression, true);
        }


        /// <summary>Performs the required operation on an object list.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Result.</returns>
        public static object Handle(Expression expression)
        {
            switch(GetQueryType(expression))
            {
                case QueryType.WHERE:
                    return Where(expression);
                case QueryType.ORDERBY:
                    return OrderBy(expression);
                case QueryType.ORDERBYDESCENDING:
                    return OrderByDescending(expression);
                case QueryType.SELECT:
                    return Select(expression);
                case QueryType.CAST:
                    return Cast(expression);
                case QueryType.CONCAT:
                    return Concat(expression);
                case QueryType.AGGREGATE:
                    return Aggregate(expression);
                case QueryType.ALL:
                    return All(expression);
                case QueryType.ANY:
                    return Any(expression);
                case QueryType.AVERAGE:
                    return Average(expression);
                case QueryType.COUNT:
                    return Count(expression);
                case QueryType.DEFAULTIFEMPTY:
                    return DefaultIfEmpty(expression);
                case QueryType.DISTINCT:
                    return Distinct(expression);
                case QueryType.ELEMENTAT:
                    return ElementAt(expression);
                case QueryType.ELEMENTATORDEFAULT:
                    return ElementAtOrDefault(expression);
                case QueryType.EXCEPT:
                    return Except(expression);
                case QueryType.FIRST:
                    return First(expression);
                case QueryType.FIRSTORDEFAULT:
                    return FirstOrDefault(expression);
                case QueryType.GROUPBY:
                    return GroupBy(expression);
                case QueryType.GROUPJOIN:
                    return GroupJoin(expression);
                case QueryType.INTERSECT:
                    return Intersect(expression);
                case QueryType.JOIN:
                    return Join(expression);
                case QueryType.LAST:
                    return Last(expression);
                case QueryType.LASTORDEFAULT:
                    return LastOrDefault(expression);
                case QueryType.LONGCOUNT:
                    return LongCount(expression);
                case QueryType.MAX:
                    return Max(expression);
                case QueryType.MIN:
                    return Min(expression);
                case QueryType.OFTYPE:
                    return OfType(expression);
                case QueryType.REVERSE:
                    return Reverse(expression);
                case QueryType.SELECTMANY:
                    return SelectMany(expression);
                case QueryType.SEQUENCEEQUAL:
                    return SequenceEqual(expression);
                case QueryType.SINGLE:
                    return Single(expression);
                case QueryType.SINGLEORDEFAULT:
                    return SingleOrDefault(expression);
                case QueryType.SKIP:
                    return Skip(expression);
                case QueryType.SKIPWHILE:
                    return SkipWhile(expression);
                case QueryType.SUM:
                    return Sum(expression);
                case QueryType.TAKE:
                    return Take(expression);
                case QueryType.TAKEWHILE:
                    return TakeWhile(expression);
                case QueryType.THENBY:
                    return ThenBy(expression);
                case QueryType.THENBYDESCENDING:
                    return ThenByDescending(expression);
                case QueryType.UNION:
                    return Union(expression);
                case QueryType.ZIP:
                    return Zip(expression);
            }

            throw new InvalidOperationException("LINQ method was not recognized.");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent entity descriptor.</summary>
        public IParsableEntity Entity
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses an expression to an SQL query.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="queryType">Query type.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Parse(Expression expression, QueryType queryType = QueryType.UNKNOWN)
        {
            return Parse(Entity.CreateQuery(), expression, queryType);
        }


        /// <summary>Parses an expression to an SQL query.</summary>
        /// <param name="query">SQL query.</param>
        /// <param name="expression">Expression.</param>
        /// <param name="queryType">Query type.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Parse(ISQLQuery query, Expression expression, QueryType queryType = QueryType.UNKNOWN)
        {
            Expression exp = expression;
            if(queryType == QueryType.UNKNOWN) { queryType = GetQueryType(expression); }

            while(!(exp is LambdaExpression))
            {
                if(exp is MethodCallExpression)
                {
                    exp = ((MethodCallExpression) exp).Arguments[1];
                }
                else if(exp is InvocationExpression)
                {
                    exp = ((InvocationExpression) exp).Expression;
                }
                else { exp = ((UnaryExpression) exp).Operand; }
            }

            int n = 0; bool strf = false;
            
            switch(queryType)
            {
                case QueryType.WHERE:
                    return query.And(_Parse(((LambdaExpression) exp).Body, query, ref n, true, ref strf));
                case QueryType.ORDERBY:
                    return query.OrderBy(new SQLField(_Parse(((LambdaExpression) exp).Body, query, ref n, true, ref strf)));
                case QueryType.ORDERBYDESCENDING:
                    return query.OrderByDesc(new SQLField(_Parse(((LambdaExpression) exp).Body, query, ref n, true, ref strf)));
            }

            return query;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an SQL operator for an expression.</summary>
        /// <param name="type">Expression type.</param>
        /// <param name="isString">Determines if the operands are of string type.</param>
        /// <param name="isNull">Determines if the right hand operand is NULL.</param>
        /// <returns>Operator string.</returns>
        private string _ToOperator(ExpressionType type, bool isString, bool isNull)
        {
            switch(type)
            {
                case ExpressionType.Add:
                    return isString ? Entity.Provider.Parser.ConcatOperator : Entity.Provider.Parser.AddOperator;
                case ExpressionType.And:
                    return Entity.Provider.Parser.BinaryAndOperator;
                case ExpressionType.AndAlso:
                    return Entity.Provider.Parser.AndOperator;
                case ExpressionType.Divide:
                    return Entity.Provider.Parser.DivideOperator;
                case ExpressionType.Equal:
                    return isNull ? Entity.Provider.Parser.EqualNullOperator : Entity.Provider.Parser.EqualOperator;
                case ExpressionType.ExclusiveOr:
                    return Entity.Provider.Parser.ExOrOperator;
                case ExpressionType.GreaterThan:
                    return Entity.Provider.Parser.GreaterOperator;
                case ExpressionType.GreaterThanOrEqual:
                    return Entity.Provider.Parser.GreaterOrEqualOperator;
                case ExpressionType.LessThan:
                    return Entity.Provider.Parser.LessOperator;
                case ExpressionType.LessThanOrEqual:
                    return Entity.Provider.Parser.LessOrEqualOperator;
                case ExpressionType.Modulo:
                    return Entity.Provider.Parser.ModuloOperator;
                case ExpressionType.Multiply:
                    return Entity.Provider.Parser.MultiplyOperator;
                case ExpressionType.Negate:
                    return Entity.Provider.Parser.NegateOperator;
                case ExpressionType.Not:
                    return Entity.Provider.Parser.NotOperator;
                case ExpressionType.NotEqual:
                    return isNull ? Entity.Provider.Parser.NotEqualNullOperator : Entity.Provider.Parser.NotEqualOperator;
                case ExpressionType.Or:
                    return Entity.Provider.Parser.BinaryOrOperator;
                case ExpressionType.OrElse:
                    return Entity.Provider.Parser.OrOperator;
                case ExpressionType.Subtract:
                    return Entity.Provider.Parser.SubtractOperator;
            }

            throw new ArgumentException("Expression of type \"" + type.ToString() + "\" could not be resolved.");
        }


        /// <summary>Evaluates the value of an expression.</summary>
        /// <param name="exp">Expression</param>
        /// <returns>Expression value.</returns>
        private static object _GetValue(Expression exp)
        {
            return Expression.Lambda<Func<object>>(Expression.Convert(exp, typeof(object))).Compile()();
        }


        /// <summary>Returns a bind variable.</summary>
        /// <param name="n">Variable index.</param>
        /// <returns>Bind variable name.</returns>
        private string _ToVar(int n)
        {
            return Entity.Provider.Parser.ToBindVariableName("p" + n.ToString());
        }


        /// <summary>Parses an expression.</summary>
        /// <param name="exp">Expression.</param>
        /// <param name="query">SQL query.</param>
        /// <param name="n">Variable index.</param>
        /// <param name="isUnary">Determines if the evaluated expression is the argument of an unary expression.</param>
        /// <param name="isString">String operation flag.</param>
        /// <returns>Where clause part.</returns>
        private string _Parse(Expression exp, ISQLQuery query, ref int n, bool isUnary, ref bool isString)
        {
            bool strf = false;

            if(exp is UnaryExpression)
            {
                if(((UnaryExpression) exp).NodeType == ExpressionType.Convert)
                {
                    return _Parse(((UnaryExpression) exp).Operand, query, ref n, true, ref strf);
                }

                string r = _Parse(((UnaryExpression) exp).Operand, query, ref n, true, ref strf);
                return "(" + _ToOperator(((UnaryExpression) exp).NodeType, false, r == "NULL") + " " + r + ")";
            }

            if(exp is BinaryExpression)
            {
                string r = _Parse(((BinaryExpression) exp).Right, query, ref n, false, ref strf);
                return "(" + _Parse(((BinaryExpression) exp).Left, query, ref n, false, ref strf) + " " + _ToOperator(((BinaryExpression) exp).NodeType, strf, r == "NULL") + " " + r + ")";
            }

            if(exp is ConstantExpression)
            {
                query.AddParameter(_ToVar(n), ((ConstantExpression) exp).Value);
                if(((ConstantExpression) exp).Value is string) { isString = true; }

                return _ToVar(n++);
            }

            if(exp is MemberExpression)
            {
                if(((MemberExpression) exp).Member is PropertyInfo)
                {
                    if(isUnary && ((MemberExpression) exp).Type == typeof(bool))
                    {
                        if(Entity.GetField(((PropertyInfo) ((MemberExpression) exp).Member).Name).DataType == SQLDataType.BOOLEAN_INTEGER)
                        {
                            return "(" + Entity.GetField(((PropertyInfo) ((MemberExpression) exp).Member).Name).ColumnExpression + Entity.Provider.Parser.NotEqualOperator + "0)";
                        }
                        else
                        {
                            query.AddParameter(_ToVar(n), true);
                            return "(" + Entity.GetField(((PropertyInfo) ((MemberExpression) exp).Member).Name).ColumnExpression + Entity.Provider.Parser.EqualOperator + _ToVar(n++) + ")";
                        }
                    }

                    if(((MemberExpression) exp).Type == typeof(string)) { isString = true; }

                    return Entity.GetField(((PropertyInfo) ((MemberExpression) exp).Member).Name).ColumnExpression;
                }

                if(((MemberExpression) exp).Member is FieldInfo)
                {
                    object v = _GetValue(exp);
                    if(v is string) { isString = true; }

                    query.AddParameter("p" + n.ToString(), v);
                    return _ToVar(n++);
                }

                throw new ArgumentException("Expression \"" + exp + "\" could not be parsed.");
            }

            if(exp is NewExpression)
            {
                object v = _GetValue(exp);
                if(v is string) { isString = true; }

                query.AddParameter("p" + n.ToString(), v);
                return _ToVar(n++);
            }

            if(exp is MethodCallExpression)
            {
                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("ToLower", new Type[0]))
                {
                    return Entity.Provider.Parser.Lowercase(_Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf));
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("ToUpper", new Type[0]))
                {
                    return Entity.Provider.Parser.Uppercase(_Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf));
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("Contains", new Type[] { typeof(string) }))
                {
                    query.AddParameter(_ToVar(n), "%" + _GetValue(((MethodCallExpression) exp).Arguments[0]).ToString() + "%");
                    return "(" + _Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf) + " LIKE " + _ToVar(n++) + ")";
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("StartsWith", new[] { typeof(string) }))
                {
                    query.AddParameter(_ToVar(n), _GetValue(((MethodCallExpression) exp).Arguments[0]).ToString() + "%");
                    return "(" + _Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf) + " LIKE " + _ToVar(n++) + ")";
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("EndsWith", new[] { typeof(string) }))
                {
                    query.AddParameter(_ToVar(n), "%" + _GetValue(((MethodCallExpression) exp).Arguments[0]).ToString());
                    return "(" + _Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf) + " LIKE " + _ToVar(n++) + ")";
                }

                if(((MethodCallExpression) exp).Method.Name == "Contains")
                {
                    Expression col, prop;

                    if(((MethodCallExpression) exp).Method.IsDefined(typeof(ExtensionAttribute)) && ((MethodCallExpression) exp).Arguments.Count == 2)
                    {
                        col = ((MethodCallExpression) exp).Arguments[0];
                        prop = ((MethodCallExpression) exp).Arguments[1];
                    }
                    else if(!((MethodCallExpression) exp).Method.IsDefined(typeof(ExtensionAttribute)) && ((MethodCallExpression) exp).Arguments.Count == 1)
                    {
                        col = ((MethodCallExpression) exp).Object;
                        prop = ((MethodCallExpression) exp).Arguments[0];
                    }
                    else { throw new ArgumentException("Method call \"" + ((MethodCallExpression) exp).Method.Name + "\" could not be resolved."); }

                    IEnumerable values = (IEnumerable) _GetValue(col);
                    string list = "";
                    bool first = true;

                    foreach(object i in values)
                    {
                        if(first) { first = false; } else { list += ", "; }

                        query.AddParameter(_ToVar(n), i);
                        list += _ToVar(n++);
                    }

                    return "(" + _Parse(prop, query, ref n, false, ref strf) + " IN (" + list + "))";
                }

                throw new ArgumentException("Method call: \"" + ((MethodCallExpression) exp).Method.Name + "\" could not be resolved.");
            }

            throw new ArgumentException("Expression of type \"" + exp.GetType().Name + "\" could not be resolved.");
        }
    }
}
