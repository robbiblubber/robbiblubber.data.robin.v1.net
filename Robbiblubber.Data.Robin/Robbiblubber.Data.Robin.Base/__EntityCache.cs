﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements an entity cache.</summary>
    internal sealed class __EntityCache: IEnumerable<__EntityDescriptor>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Entities.</summary>
        private static Dictionary<Type, __EntityDescriptor> _Items = new Dictionary<Type, __EntityDescriptor>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public __EntityDescriptor this[Type t]
        {
            get 
            {
                if(!_Items.ContainsKey(t)) { return Register(t); }
                return _Items[t];
            }
        }


        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Registers a type.</summary>
        /// <param name="t">Type.</param>
        public __EntityDescriptor Register(Type t)
        {
            if(_Items.ContainsKey(t)) { return _Items[t]; }
            if(t.GetCustomAttribute<IgnoreAttribute>() != null) { return null; }

            if(t.GetCustomAttribute<EntityAttribute>() != null)
            {
                _Items.Add(t, new __EntityDescriptor(t));
                return _Items[t];
            }

            return null;
        }


        /// <summary>Registers all types in an assembly.</summary>
        /// <param name="asm"></param>
        public void Register(Assembly asm)
        {
            foreach(Type i in asm.GetTypes()) { Register(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerator<__EntityDescriptor>                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<__EntityDescriptor> IEnumerable<__EntityDescriptor>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerator                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
