﻿using System;
using System.Reflection;
using Robbiblubber.Data.Robin.Base;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a database-bound primary key field.</summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class PrimaryAttribute: FieldAttribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="columnAlias">Column alias.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="size">Data size.</param>
        public PrimaryAttribute(string columnName = null, string columnAlias = null, DataType dataType = DataType.AUTO, int size = 0)
        {
            ColumnName = columnName;
            ColumnAlias = columnAlias;
            DataType = dataType;
            Size = size;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Determines if the primary key field is auto-increment.</summary>
        public bool AutoIncrement = false;

        /// <summary>Primary key generator class name.</summary>
        public string GeneratorClass = null;

        /// <summary>Primary key generator method name.</summary>
        /// <remarks>Generator methods must be static.</remarks>
        public string GeneratorMethod = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the primary key generator delegate.</summary>
        /// <param name="t">Type.</param>
        public CreateNextPrimary GetGenerator(Type t)
        {
            if(string.IsNullOrWhiteSpace(GeneratorMethod)) return null;

            try
            {
                Type gen = null;
                try
                {
                    if(!string.IsNullOrWhiteSpace(GeneratorClass)) { gen = Type.GetType(GeneratorClass); }
                }
                catch(Exception) {}

                if(gen == null) { gen = t; }

                MethodInfo m = gen.GetMethod(GeneratorMethod, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                
                if(m != null) return (CreateNextPrimary) Delegate.CreateDelegate(typeof(CreateNextPrimary), m);
            }
            catch(Exception) {}

            return null;
        }
    }
}
