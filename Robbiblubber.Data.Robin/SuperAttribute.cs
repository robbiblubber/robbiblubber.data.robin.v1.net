﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a database-bound class as a superclass for joined classes.</summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class SuperAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SuperAttribute()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="childEntities">Child entity names.</param>
        public SuperAttribute(params string[] childEntities)
        {
            ChildEntities = childEntities;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="superType">Super entity type.</param>
        /// <param name="childEntities">Child entity names.</param>
        public SuperAttribute(SuperType superType, params string[] childEntities)
        {
            SuperType = superType;
            ChildEntities = childEntities;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Super entity type.</summary>
        public SuperType SuperType = SuperType.MATERIAL;


        /// <summary>Child entity names.</summary>
        public string[] ChildEntities = null;
    }
}
