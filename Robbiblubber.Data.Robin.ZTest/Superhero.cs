﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robbiblubber.Data.Robin.Test
{
    /// <summary>This class represents a superhero.</summary>
    [Entity(TableName = "SUPERHEROS")]
    public class Superhero: Atom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the superhero ID.</summary>
        [Primary(AutoIncrement = true)]
        public int ID { get; protected set; }


        /// <summary>Gets the superhero name.</summary>
        [Field]
        public string Name { get; set; }
    }
}
