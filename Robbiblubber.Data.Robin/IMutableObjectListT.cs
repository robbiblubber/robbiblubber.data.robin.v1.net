﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Robin
{
    /// <summary>Mutable object lists implement this interface.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public interface IMutableObjectList<T>: IMutableList<T>, IExtensibleObjectList<T>, IExtensibleList<T>, IImmutableObjectList<T>, IImmutableList<T>, IOrderedQueryable<T>, IQueryable<T>, IEnumerable<T>, IEnumerable
    {}
}
