﻿using System;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>Marshals a field value.</summary>
    /// <param name="direction">Direction.</param>
    /// <param name="value">Input value.</param>
    /// <returns>Marshalled output value.</returns>
    public delegate object MarshalField(MarshalDirection direction, object value);
}
