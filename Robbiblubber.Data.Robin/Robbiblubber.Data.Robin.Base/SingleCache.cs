﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class implements a single cache instance.</summary>
    public sealed class SingleCache: __ICache, ICacheProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cache.</summary>
        private Dictionary<Type, Dictionary<object, __CacheElement>> _Cache = new Dictionary<Type, Dictionary<object, __CacheElement>>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICache                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the cache consistency options.</summary>
        public CacheConsistency Consitency { get; set; } = CacheConsistency.DEFAULT;


        /// <summary>Gets or sets the cache time-to-live.</summary>
        public TimeSpan TTL { get; set; } = new TimeSpan(0, 20, 0);


        /// <summary>Gets an array of types currently cached.</summary>
        public Type[] Types 
        { 
            get { return _Cache.Keys.ToArray(); }
        }


        /// <summary>Returns the object of a given type with the given primary key.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns an object from the cache. Returns NULL if the cache does not contain a matching object.</returns>
        public ICacheResult this[object obj]
        {
            get 
            { 
                return this[obj.GetType(), Manager._EntityCache[obj.GetType()].PrimaryKey.GetValue(obj, false)]; 
            }
        }


        /// <summary>Returns the object of a given type with the given primary key.</summary>
        /// <param name="t">Type.</param>
        /// <param name="pk">Primary key.</param>
        /// <returns>Returns an object from the cache. Returns NULL if the cache does not contain a matching object.</returns>
        public ICacheResult this[Type t, object pk]
        {
            get
            {
                if(!_Cache.ContainsKey(t)) { return new __CacheElement(t, null); }
                if(!_Cache[t].ContainsKey(pk)) { return new __CacheElement(t, null); }

                return _Cache[t][pk];
            }
        }


        /// <summary>Returns if the cache contains a given object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <returns>Returns TRUE if the cache contains the object, otherwise returns FALSE.</returns>
        public bool Contains(object obj)
        {
            return Contains(obj.GetType(), Manager._EntityCache[obj.GetType()].PrimaryKey.GetValue(obj, false));
        }


        /// <summary>Returns if the cache contains an object of a given type with the given primary key.</summary>
        /// <param name="t">Type.</param>
        /// <param name="pk">Primary key.</param>
        /// <returns>Returns TRUE if the cache contains a matching object, otherwise returns FALSE.</returns>
        public bool Contains(Type t, object pk)
        {
            if(pk == null) { return false; }
            if(!_Cache.ContainsKey(t)) { return false; }

            return _Cache[t].ContainsKey(pk);
        }


        /// <summary>Returns the last read time of the cache elements of a type.</summary>
        /// <param name="t">Type.</param>
        /// <returns>Last read time.</returns>
        public DateTime GetReadTime(Type t)
        {
            DateTime rval = DateTime.Now;

            if(_Cache.ContainsKey(t))
            {
                foreach(ICacheResult i in _Cache[t].Values)
                {
                    if(i.LastRead < rval) { rval = i.LastRead; }
                }
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __ICache                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the cache dictionary.</summary>
        Dictionary<Type, Dictionary<object, __CacheElement>> __ICache.CacheDictionary 
        { 
            get { return _Cache; }
        }
    }
}
