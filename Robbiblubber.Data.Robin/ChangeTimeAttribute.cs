﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This attribute denotes a database-bound field that contains the last change time.</summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class ChangeTimeAttribute: FieldAttribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="columnName">Column name.</param>
        /// <param name="columnAlias">Column alias.</param>
        /// <param name="dataType">Data type.</param>
        /// <param name="size">Data size.</param>
        public ChangeTimeAttribute(string columnName = null, string columnAlias = null, DataType dataType = DataType.AUTO, int size = 0)
        {
            ColumnName = columnName;
            ColumnAlias = columnAlias;
            DataType = dataType;
            Size = size;
        }
    }
}
