﻿using System;



namespace Robbiblubber.Data.Robin
{
    /// <summary>This class defines types of super entities.</summary>
    public enum SuperType
    {
        /// <summary>The super entity is a base class with a table.</summary>
        MATERIAL = 1,
        /// <summary>The super entity is a base class without a table.</summary>
        VIRTUAL = 2,
        /// <summary>The super entity is a base class for entities that share a single table.</summary>
        INTEGRATED = 4
    }
}
