﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.Robin.Timing;
using Robbiblubber.Data;



namespace Robbiblubber.Data.Robin.Base
{
    /// <summary>This class contains cached object data.</summary>
    internal sealed class __CacheElement: ICacheResult
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Entity descriptor.</summary>
        private __EntityDescriptor _Entity = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="t">Type.</param>
        /// <param name="obj">Cached object.</param>
        public __CacheElement(Type t, object obj)
        {
            Object = obj;
            _Entity = Manager._EntityCache[t];
            Update();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the stored values for the cached object.</summary>
        public Dictionary<string, object> StoredValues
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        

        /// <summary>Restores the object using the cached values.</summary>
        /// <param name="obj">Object instance.</param>
        public void Restore(object obj = null)
        {
            if(obj != null) { Object = obj; }

            if(Object != null)
            {
                foreach(__FieldDescriptor i in _Entity.Fields) { i.SetValue(Object, StoredValues[i.Name], false); }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICacheResult                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the cached object.</summary>
        public object Object
        {
            get; private set;
        } = null;


        /// <summary>Gets if the cached object has been changed.</summary>
        public bool HasChanges
        {
            get
            {
                if(Object == null) { return false; }

                foreach(__FieldDescriptor i in _Entity.Fields)
                {
                    if(StoredValues[i.Name] == null)
                    {
                        if(i.GetValue(Object, false) != null) { return true; }
                    }
                    else if(!StoredValues[i.Name].Equals(i.GetValue(Object, false))) { return true; }
                }

                return false;
            }
        }


        /// <summary>Gets or sets the read time for the cached object.</summary>
        public DateTime LastRead
        {
            get; private set;
        } = DateTime.MinValue;


        /// <summary>Gets if the cached object is current.</summary>
        public bool IsCurrent 
        { 
            get
            {
                if(Object == null) return false;
                if(Manager.Cache.Consitency == CacheConsistency.FULL) return false;
                if(Manager.Cache.Consitency == CacheConsistency.NONE) return true;
                
                return ((LastRead + Manager.Cache.TTL) < DateTime.Now);
            }
        }


        /// <summary>Gets a list of fields with changes.</summary>
        public IEnumerable<string> ChangedFieldNames
        {
            get
            {
                if(Object == null) return new string[0];

                List<string> rval = new List<string>();
                foreach(__FieldDescriptor i in _Entity.Fields)
                {
                    if(i.GetValue(Object, false) == null)
                    {
                        if(StoredValues[i.Name] != null) { rval.Add(i.Name); }
                    }
                    else if(i.AlwaysWrite || (!i.GetValue(Object, false).Equals(StoredValues[i.Name]))) { rval.Add(i.Name); }
                }

                return rval;
            }
        }


        /// <summary>Updates the stored values for the cached object.</summary>
        public void Update()
        {
            Update(null);
        }


        /// <summary>Updates the stored values for the cached object.</summary>
        /// <param name="obj">Object instance.</param>
        public void Update(object obj)
        {
            if(obj is IDataReader)
            {
                Update(null, (IDataReader) obj, false);
                return;
            }

            if(obj != null) { Object = obj; }

            if(Object != null)
            {
                StoredValues = new Dictionary<string, object>();
                foreach(__FieldDescriptor i in _Entity.Fields) { StoredValues.Add(i.Name, i.GetValue(Object, false)); }
                LastRead = ServerTime.Now;
            }
        }


        /// <summary>Updates the stored values for the cached object.</summary>
        /// <param name="obj">Object instance.</param>
        /// <param name="re">Data.</param>
        /// <param name="force">Determines if refresh will ignore changes.</param>
        public void Update(object obj, IDataReader re, bool force)
        {
            if(obj != null) { Object = obj; }

            if(Object == null)
            {
                Object = Activator.CreateInstance(_Entity.Type, true);

                foreach(__FieldDescriptor i in _Entity.Fields)
                {
                    i.SetValue(Object, re);
                }

                if(!((__ICache) Manager.Cache).CacheDictionary.ContainsKey(_Entity.Type)) { ((__ICache) Manager.Cache).CacheDictionary.Add(_Entity.Type, new Dictionary<object, __CacheElement>()); }
                ((__ICache) Manager.Cache).CacheDictionary[_Entity.Type].Add(_Entity.PrimaryKey.GetValue(Object, false), this);
            }
            else if((!force) && HasChanges) { return; }
            else if((!force) && (_Entity.ChangeTime != null) && ((DateTime) _Entity.ChangeTime.GetValue(Object, false) >= (DateTime) _Entity.ChangeTime.Marshal(MarshalDirection.IN, re.GetValue(re.GetOrdinal(_Entity.ChangeTime.ColumnAlias))))) { return; }
            else
            {
                foreach(__FieldDescriptor i in _Entity.Fields)
                {
                    i.SetValue(Object, re);
                }
            }

            _Entity.Initialize(Object, force);
            Update();
        }


        /// <summary>Updates the stored values for the cached object.</summary>
        /// <param name="re">Data.</param>
        /// <param name="force">Determines if refresh will ignore changes.</param>
        public void Update(IDataReader re, bool force)
        {
            if(re == null)
            {
                Update((object) null);
                return;
            }

            Update(null, re, force);
        }


        /// <summary>Removes the stored object from the cache.</summary>
        public void Remove()
        {
            if(Object == null) return;
            if(!Manager.Cache.Contains(Object)) return;

            ((__ICache) Manager.Cache).CacheDictionary[_Entity.Type].Remove(_Entity.PrimaryKey.GetValue(Object, false));
        }


        /// <summary>Gets the old value of a field.</summary>
        /// <param name="field">Field.</param>
        /// <returns>Value.</returns>
        public object GetOldValue(string field)
        {
            return StoredValues[field];
        }
    }
}
