﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Data.Robin.Parsing
{
    /// <summary>Basic list interface.</summary>
    internal interface __IList
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the underlying list for this instance.</summary>
        /// <returns>List.</returns>
        IList GetList();


        /// <summary>Sets the underlying list for this instance.</summary>
        /// <param name="list">List.</param>
        void SetList(IList list);


        /// <summary>Gets the sorting for this instance.</summary>
        /// <returns>Sorting.</returns>
        List<IComparer> GetSorting();
    }
}
